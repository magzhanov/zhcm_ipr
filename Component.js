sap.ui.define([
    'sap/ui/core/UIComponent',
    'sap/ui/model/json/JSONModel',
    'zhcm_IPR/localService/mockserver'
], function(UIComponent, JSONModel, MockServer) {
    'use strict';

    return UIComponent.extend('zhcm_IPR.Component', {

        metadata: {
            manifest: 'json'
        },

        init: function () {

            var sListMode = "SingleSelectMaster",
                sListItemType = "Inactive";
            if (sap.ui.Device.system.phone) {
                sListMode = "None";
                sListItemType = "Active";
            }

            var deviceModel = new JSONModel({
                isTouch: sap.ui.Device.support.touch,
                isNoTouch: !sap.ui.Device.support.touch,
                isPhone: sap.ui.Device.system.phone,
                isNoPhone: !sap.ui.Device.system.phone,
                listMode: sListMode,
                listItemType: sListItemType
            });
            deviceModel.setDefaultBindingMode("OneWay");
            this.setModel(deviceModel, "device");
            
            var oAppData = new JSONModel({
                initialReadRequired: true,
                overviewFilters: {
                    PernrMemId: "",
                    Year: "",
                    ProcStatus: "",
                    Subord: "",
                    MySubordsFilterLoading: false
                }
            });
            this.setModel(oAppData, "AppData");

            UIComponent.prototype.init.apply(this, arguments);
            this.getRouter().initialize();
        }
    });
});