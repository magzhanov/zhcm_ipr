sap.ui.define([
    "sap/ui/core/util/MockServer",
    "zhcm_IPR/utils/Config"
], function(MockServer, Config) {
    "use strict";

    return {

        init: function() {
            var oMockServer = new MockServer({
                rootUri: Config.PM_IPR_SERVICE
            });

            oMockServer.simulate("../webapp/localService/metadata.xml", {
                sMockdataBaseUrl: "../webapp/localService/mockdata",
                bGenerateMissingMockData: true
            });

            oMockServer.start();

        }

    };

});