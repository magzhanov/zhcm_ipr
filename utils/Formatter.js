sap.ui.define(["./Config"],
    function (Config) {
        "use strict";

        var Formatter = {

            tableItemType: function (sNextInterface) {
                if (sNextInterface) {
                    return "Navigation";
                } else {
                    return "Inactive";
                }
            },

            onlyActiveVisible: function (bShowCompleted, sIndicator) {
                var bVisible = false;
                if (bShowCompleted == true) {
                    bVisible = true;
                } else {
                    if (sIndicator !== Config.Indicators.Completed) {
                        bVisible = true;
                    }
                }

                return bVisible;
            },

            taskStatusIcon: function (sIndicator) {
                switch (sIndicator) {
                    case Config.Indicators.TakeAction:
                        return "sap-icon://message-error";
                    case Config.Indicators.Pending:
                        return "sap-icon://pending";
                    case Config.Indicators.Completed:
                        return "sap-icon://complete";
                }
            },

            taskStatusTooltip: function (sIndicator) {
                switch (sIndicator) {
                    case Config.Indicators.TakeAction:
                        return "Обработать";
                    case Config.Indicators.Pending:
                        return "В работе";
                    case Config.Indicators.Completed:
                        return "Завершено";
                }
            },

            taskStatusColor: function (sIndicator) {
                switch (sIndicator) {
                    case Config.Indicators.TakeAction:
                        return "zRedTakeAction";
                    case Config.Indicators.Pending:
                        return "zBluePending";
                    case Config.Indicators.Completed:
                        return "zGreenCompleted";
                }
            },

            getRzlStr: function(str){
                return String(parseFloat(str, 10) || 0.0) + '%';
            },

            createStep2enabled: function(pickedNum, min, max, bLocked, bIndicator){
                if (typeof bIndicator !== 'boolean') {
                    bIndicator = true;
                }

                var iMin = parseInt(min) || 0,
                    iMax = parseInt(max) || 0,
                    iNum = parseInt(pickedNum) || 0,
                    bEnabled = false;

                if (iMin && !iMax) {
                    bEnabled = iNum >= iMin;
                }
                if (!iMin && iMax) {
                    bEnabled = iNum <= iMax;
                }
                if (iMin && iMax) {
                    bEnabled = iNum >= iMin && iNum <= iMax;
                }
                if (bLocked && bIndicator) {
                    bEnabled = false;
                }

                return bEnabled;
            },

            expandIcon: function (bExpanded) {
                if (bExpanded) {
                    return 'sap-icon://navigation-down-arrow';
                } else { 
                    return 'sap-icon://navigation-right-arrow';
                }
            },

            untilDate: function (yyyymmdd) {
                if (yyyymmdd) {
                    return 'до ' + yyyymmdd.substr(6, 2) + '.' + 
                                   yyyymmdd.substr(4, 2) + '.' + 
                                   yyyymmdd.substr(0, 4);
                }
            },

            strToInt: function(str) {
              if (str !== null && str !== undefined){
                return parseInt(str, 10) || 0;
              }
              return 0;
            },

            textNumberVisible: function(str) {
                var bVisible = true,
                    n = parseInt(str, 10) || 0;
                if (n <= 0) {
                    bVisible = false;
                }
                return bVisible;
            },

            overviewHeaderWarningVisible: function (iActiveTasks, aPernrsAmList, sCurrentPernr) {
                var bResult = false,
                    sPernr;

                if (iActiveTasks !== undefined && aPernrsAmList && sCurrentPernr) {
                    bResult = !!aPernrsAmList.reduce(function (acc, oPernr) {
                        sPernr = Object.keys(oPernr)[0];
                        if (sPernr !== sCurrentPernr) {
                            acc += oPernr[sPernr];
                        }
                        return acc;
                    }, 0);
                }
                return bResult;
            },
            colListItemsTextLong: function(sPLANS_TXT, sORGEH_TXT) {
                var textLength = sPLANS_TXT.length + sORGEH_TXT.length;
                if (textLength > 80) {
                    return true;
                }else{
                    return false;
                }
            },
            colListItemsTextSmall: function(sPLANS_TXT, sORGEH_TXT) {
                var textLength = sPLANS_TXT.length + sORGEH_TXT.length;
                if (textLength > 80) {
                    return false;
                }else{
                    return true;
                }
            },

            executionResultVisible: function(oModel) {
                if (oModel && oModel.length > 0) {
                    for (var i = 0; i < oModel.length; i++) {
                        if (oModel[i].RZL !== "-") {
                            return true;
                        }
                    }
                }
                return false;
            },

            strGetLevel: function(oData, str) {
              if (oData === null || oData === undefined) {
                oData = this.oView.getModel('levels').getData();
              }
              if (oData && oData.length > 0) {
                    var n = parseInt(str, 10) || 0;
                    for(var i = 0; i < oData.length; i++) {
                    if (oData[i].id === n) {
                      return oData[i].LEVEL;
                    }
                  }
              }
              return "";
            },
                       
            buttonVisible: function(sRole, sEnabled, sApprove, bInvert, bMoreApprovals) {
                if (typeof bMoreApprovals !== 'boolean') {
                    bMoreApprovals = false;
                }
                if ((sRole && sRole === "MT") || bMoreApprovals) {   // Наставник
                    return false;
                } else {
                    if (sEnabled !== undefined && sApprove !== undefined) {
                        if (bInvert !== undefined && bInvert === true) {
                            return sEnabled && !sApprove;
                        } else {
                            return sEnabled && sApprove;
                        }
                    }                    
                }
                return true;                
            },
            
            tutorEnabled: function(sRole, bIndicatorFlag) {
                if (typeof bIndicatorFlag !== 'boolean') {
                    bIndicatorFlag = true;
                }
                if (sRole && sRole === "ZM" && bIndicatorFlag) {  // Начальник
                    return true;
                }
                return false;
            },

            tutorPanelVisible: function(sRole, bIndicatorFlag, sFio) {
                if (bIndicatorFlag && ((sFio && sFio.length > 0) || (sRole && sRole === "ZM"))) {  
                    return true;
                }
                return false;
            },

            
        };

        return Formatter;

    }, true);