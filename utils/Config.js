sap.ui.define([], function () {
    "use strict";

    var Config = {
        ZHCM_KPI_CARDS_SERVICE: "/sap/opu/odata/sap/ZHCM_PM_0443_CRDS_LIST_SRV/",
        PM_IPR_SERVICE: "/sap/opu/odata/sap/ZHCM_PM_0681_SRV/",
        PM_360_SERVICE: "/sap/opu/odata/sap/ZHCM_PM_0650_SRV/",
        _ZHCM_KPI_CARDS_SERVICE: "http://sms00990:8010/sap/opu/odata/sap/ZHCM_PM_0443_CRDS_LIST_SRV/",
        _PM_IPR_SERVICE: "http://sms00990:8010/sap/opu/odata/sap/ZHCM_PM_0681_SRV/",
        _PM_360_SERVICE: "http://sms00990:8010/sap/opu/odata/sap/ZHCM_PM_0650_SRV/",

        Indicators: {
            TakeAction: "1",
            Pending: "2",
            Completed: "3"
        },

        ReportKey360: "ZHCM_PM_XXXX_REP_IND",

        valueHelpCust: [
            {
                inputId: "filterSubord",
                key: "PERNR",
                descrKey: "FIO",
                model: "MySubordsUnique",
                cols: [
                    {label: "ТН", template: "PERNR"},
                    {label: "ФИО", template: "FIO"},
                    {label: "Штатная должность", template: "PLANS_TXT", demandPopin: true},
                    {label: "Организационная единица", template: "ORGEH_TXT", demandPopin: true}
                ]
            }],
    };

    return Config;
});