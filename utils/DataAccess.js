sap.ui.define([
    "zhcm_IPR/utils/Config",
    "sap/ui/model/odata/v2/ODataModel",
    "sap/ui/model/Filter"
],
    function (Config, ODataModel, Filter) {
        "use strict";

        var DataAccess = {
            getIprService: function () {
                if (!this._oDataModelIpr) {
                    this._oDataModelIpr = new ODataModel(Config.PM_IPR_SERVICE, {
                        useBatch: false
                    });
                }

                return this._oDataModelIpr;
            },

            getInstrText: function (sView, sUiElement, sIdEvalProc) {
                var oInstrData = this.getOwnerComponent().getModel("Instructions").getData().d,
                    sText = "";

                if (oInstrData && oInstrData.hasOwnProperty("results")) {
                    $.each(oInstrData.results, function (iInd, oObj) {
                        if (oObj.INTERFACE_ID === sView &&
                            oObj.UI_ELEMENT === sUiElement &&
                            oObj.ID_EVAL_PROC === sIdEvalProc) {

                            sText = oObj.TEXT;
                        }
                    });

                    return sText;
                }
            },

            executeRead: function (oDataModel, sReadUrl, mUrlParams, aFilters) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    var resultData = oData.results || oData;
                    d.resolve(resultData);
                };
                var e = function (oError) {
                    d.reject(JSON.parse(oError.responseText));
                };
                var mParameters = {
                    urlParameters: mUrlParams,
                    filters: aFilters,
                    success: s,
                    error: e
                };

                oDataModel.read(sReadUrl, mParameters);
                return d.promise();
            },

            getUserMemId: function () {
                var oDataModel = new ODataModel(Config.ZHCM_KPI_CARDS_SERVICE, {
                    useBatch: false
                });

                var sReadUrl = "/PernrsMemIdSet";
                return this.executeRead(oDataModel, sReadUrl);
            },

            getMyIpr: function (sPernrMemId) {
                var sReadUrl = "/MyIprSet",
                    aFlt = [new Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getTutorShips: function (sPernrMemId) {
                var sReadUrl = "/MyTutorShipsSet",
                    aFlt = [new Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getSubDivs: function (sPernrMemId) {
                var sReadUrl = "/MySubDivsSet",
                    aFlt = [new Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getReportProcSet: function (sPernrMemId) {
                var sReadUrl = "/ReportProcSet",
                    aFlt = [
                        // new Filter({
                        //     path: "PERNR_MEMID",
                        //     operator: sap.ui.model.FilterOperator.EQ,
                        //     value1: sPernrMemId
                        // })
                    ];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getReportOrgehSet: function (sIdDevProc) {
                var sReadUrl = "/ReportORGEHSet",
                    aFlt = [
                        new Filter({
                            path: "ID_DEVPLAN_PROC",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: String(sIdDevProc)
                        })
                    ];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },
            getReportFioSet: function (sIdDevProc) {
                var sReadUrl = "/ReportFIOSet",
                    aFlt = [
                        new Filter({
                            path: "ID_DEVPLAN_PROC",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: String(sIdDevProc)
                        })
                    ];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },
            getReportDevTypeSet: function (sIdDevProc) {
                var sReadUrl = "/ReportDevTypeSet",
                    aFlt = [
                        new Filter({
                            path: "ID_DEVPLAN_PROC",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: String(sIdDevProc)
                        })
                    ];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getMySubords: function (sPernrMemId) {
                var sReadUrl = "/MySubordsSet",
                    aFlt = [new Filter({
                        path: "PERNR_MEMID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sPernrMemId
                    })];

                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getIprData: function (sAppraisalId, sPernrMemId) {
                var sExpand = "Competencies,Messages,Competencies/DevTypes,Competencies/DevTypes/DevMethods,Tutors"; // ,LevelScale
                var that = this;

                return this.getIprService().metadataLoaded()
                    .then(function () {
                        return that.getIprService().createKey("/IprDocSet", {
                            PERNR_MEMID: sPernrMemId,
                            APPRAISAL_ID: sAppraisalId
                        });
                    })
                    .then(function (sPath) {
                        return that.executeRead(that.getIprService(), sPath, {"$expand": sExpand});
                    });
            },

            getFeed: function (sAppraisalId) {
                var sReadUrl = "/FeedSet",
                    aFlt = [new Filter({
                        path: "APPRAISAL_ID",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sAppraisalId
                    })];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            getChipData: function () {
                var sReadUrl = "/ChipSet";
                return this.executeRead(this.getIprService(), sReadUrl);
            },

            getMethodsForComp: function (idDevType, idLib, idComp, idLevel) {
                var sReadUrl = "/LibrarySet",
                    aFlt = [
                        new Filter({
                            path: "ID_DEV_TYPE",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: idDevType
                        }),
                        new Filter({
                            path: "COMP_ID",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: idComp
                        }),
                        new Filter({
                            path: "ID_LIBRARY",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: idLib
                        }),
                        new Filter({
                            path: "LEVEL",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: idLevel
                        })
                    ];
                return this.executeRead(this.getIprService(), sReadUrl, {}, aFlt);
            },

            sendIprData: function (oPaylaod) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    var aMessages = [];
                    if (oData.Messages && oData.Messages.results && oData.Messages.results.length > 0) {
                        aMessages = oData.Messages.results;
                        d.reject(aMessages);
                    } else {
                        d.resolve(oData);
                    }
                };

                var e = function (oError) {
                    d.reject(oError);
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getIprService().create("/IprDocSet", oPaylaod, mParameters);
                return d.promise();
            },

            sendFeedComment: function (oPaylaod) {
                var d = $.Deferred();

                var s = function (oData, oResponse) {
                    if(oData.APPRAISAL_ID){
                        d.resolve(oData);
                    } else {
                        d.reject("Ошибка добавления комментария");
                    }
                };

                var e = function (oError) {
                    d.reject(oError);
                };

                var mParameters = {
                    urlParameters: {},
                    success: s,
                    error: e
                };

                this.getIprService().create("/FeedSet", oPaylaod, mParameters);
                return d.promise();
            },

            getPrintFormatFromUser: function () {
                var d = $.Deferred(),
                    _sPrintFormat = null;

                var dialog = new sap.m.Dialog({
                    title: 'Формат отчета',
                    type: 'Message',
                    content: [
                        new sap.m.RadioButtonGroup("rgbFormat", {
                                buttons: [
                                    new sap.m.RadioButton("rbPDF", {text: "PDF"}),
                                    new sap.m.RadioButton("rbXLS", {text: "Excel"}),
                                ]
                            }
                        )
                    ],
                    beginButton: new sap.m.Button({
                        text: 'Ок',
                        type: "Emphasized",
                        press: function () {
                            switch (sap.ui.getCore().byId('rgbFormat').getSelectedIndex()) {
                                case 0:
                                    _sPrintFormat = "PDF";
                                    break;
                                case 1:
                                    _sPrintFormat = "EXCEL";
                            }
                            dialog.close();
                            d.resolve(_sPrintFormat);
                        }
                    }),
                    endButton: new sap.m.Button({
                        text: 'Отменить',
                        type: "Reject",
                        press: function () {
                            d.reject();
                            dialog.close();
                        }
                    }),
                    afterClose: function () {
                        dialog.destroy();
                    }
                });

                dialog.open();
                return d.promise();
            },

            getPrintform360: function (sPernr, sEvalProc360, sFormat) {
                sFormat = sFormat || "PDF";
                var sPernrsString = '';
                var sUrlParams = "REPORTKEY='" + Config.ReportKey360 +
                                 "',PERNR='" + sPernr +
                                 "',ID_EVAL_PROC='" + sEvalProc360 +
                                 "',PERNRS_STRING='" + sPernrsString +
                                 "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_360_SERVICE + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
            },

            getPrintformIPR: function (sPernr, sEvalProc, sFormat) {
                sFormat = sFormat || "PDF";
                var sUrlParams = "PERNR='" + sPernr +
                    "',ID_EVAL_PROC='" + sEvalProc +
                    "',FORMAT='" + sFormat + "'",

                    sPath = window.location.origin + Config.PM_IPR_SERVICE + "ReportSet(" + sUrlParams + ")/$value";

                window.open(sPath);
            },

            getStatisticalReport: function(oUrlParams){
                var sPath = Config.PM_IPR_SERVICE + 
                            '/ReportStatSet(ID_DEVPLAN_PROC=\''+ oUrlParams.CbSelProcIPR +
                            '\',PERNR_STR=\''+ oUrlParams.CbSelUser +
                            '\',ORGEH_STR=\''+ oUrlParams.CbSelOrgeh +
                            '\',DEV_TYPE_STR=\''+oUrlParams.CbSelType +'\')/$value';

                window.open(sPath);
            },

            getReportAnonymityCheck : function (sPernr, sIdEvalProc, sAppraisal) {
                var sUrlParams = "PERNR='" + sPernr +
                                "',ID_EVAL_PROC='" + sIdEvalProc +
                                "',APPRAISAL_ID='" + sAppraisal +
                                "'";
                var sReadUrl = "/ReportAnonymCheckSet(" + sUrlParams + ")";
                return this.executeRead(this.getIprService(), sReadUrl, {}, {});
            },

        };


        return DataAccess;
    });