sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";
    
    var CompetencyTile = Control.extend("zhcm_IPR.customControls.CompetencyTile", {
        metadata: {
            properties: {
                name: {type: "string"},
                rating: {type: "string"},
                picked: {type: "boolean"},
                noRatings: {type: "boolean"},
                draggable: {type: "boolean", defaultValue: true},
                prevIprFound: {type: "boolean"},
            }
        },

        renderer: function (oRM, oControl) {
            if (oControl.getDraggable()) {
                oRM.write("<div draggable='true'");
            } else {
                oRM.write("<div draggable='false'");
            }
            oRM.writeControlData(oControl);
            oRM.addClass("zCompetencyTile");
            oRM.writeClasses();
            oRM.write(">");

            var n = parseFloat(oControl.getRating()) || 0.0,
                noRatings = oControl.getNoRatings() || false,
                bPrevIprFound = oControl.getPrevIprFound() || false;

            if (!noRatings) {
                    oRM.write('<div>');
                    oRM.write(oControl.getName());
                    oRM.write("</div>");

                    oRM.write('<div style="color: #999999">');            
                    oRM.write('Cредняя оценка ' + n.toFixed(1));
                    oRM.write("</div>");
            } else {
                oRM.write('<div style="line-height: 1.75rem;">');
                oRM.write(oControl.getName());
                oRM.write("</div>");
                
                if (bPrevIprFound) {
                    oRM.write('<div style="color: #999999">');            
                    oRM.write('Процент выполнения ' + n.toFixed(1));
                    oRM.write("</div>");
                }
            }

            oRM.write('</div>');
        },

    });

    return CompetencyTile;
});