sap.ui.define([
    "sap/ui/core/Control",
    "sap/m/TextArea",
    "sap/m/SegmentedButton",
    "sap/m/Input",
    "sap/m/SegmentedButtonItem",
], function (Control, TextArea, SegmBtn, Input, SBI) {
    "use strict";
    /**
     * Контрол, отображающий просмотр/заполнение результата
     * по методу развития. В зависимости от настроек процедуры
     * отображает либо: 1. TextArea для ввода результата
     *                  2. SegmenttedButton с 3-мя вариантами
     *                  3. Оба сразу
     */
    //@ts-check
    var CompReportTile = Control.extend("zhcm_IPR.customControls.CompReportTile", {
        metadata: {
            properties: {
                rowIID: {type: "string"},

                procResult: {type: "string"},   
                procResultNoValue: {type: "boolean"},             
                compWeight: {type: "string"},
                showCompWeight: {type: "boolean"},

                compDescr: {type: "string"},
                untilDate: {type: "string"},

                shortReport: {type: "boolean"},
                detailedReport: {type: "boolean"},

                shortOblig: {type: "boolean"},
                detailedOblig: {type: "boolean"},

                devResultShort: {type: "string", defaultValue: "0"},
                devResultText: {type: "string"},

                enabled: {type: "boolean"}, // отличие от editable - тут 3 кнопки в компетенции пропадают, а там - становятся не редактируемы
                editable: { type: "boolean" },

                isPhone: {type: "boolean", defaultValue: false},

                hasErrorBorder: {type: "boolean", defaultValue: false},
                hasErrorBorderSegmBtn: {type: "boolean", defaultValue: false},
                hasErrorBorderTextArea: {type: "boolean", defaultValue: false},

                procRZL: {type: "string"},
                methID: {type: "string"},

                hasEnterFirst: {type: "boolean", defaultValue: false},

                prcHigh: {type: "string"},
                prcLow: {type: "string"},
                prcStep: {type: "string"}
            },

            aggregations: {
                _resTextArea: {type: "sap.m.TextArea", multiple: false},
                _resSegmBtn: {type: "sap.m.SegmentedButton", multiple: false},
                _procInput: {type: "sap.m.Input", multiple: false},
            },

            events: {
                onPercentChange: {}
            },

        },

        COLOR_CLASSES: {
            Neutral: "",
            Overdue: "zReportTitleOverdue",
            WithinMonth: "zReportTitleMonth",
        },

        SEGM_BUTTON_ITEMS: [
            new SBI({key: "1", text: "Выполнено"}),
            new SBI({key: "0", text: "Не выбрано"}),
            new SBI({key: "2", text: "Не выполнено"})
        ],

        constructor: function (mSettings) {
            Control.apply(this, arguments);

            if (mSettings.compDescr) {

                console.log(mSettings);

                var oSegBtn = new SegmBtn({
                    selectedKey: mSettings.devResultShort,
                    items: this.SEGM_BUTTON_ITEMS,
                    enabled: mSettings.editable
                });
                oSegBtn.addStyleClass("zResSegmBtn");

                this.setAggregation("_resSegmBtn", oSegBtn);

                var oTextArea = new TextArea({
                    width: "100%",
                    placeholder: "Опишите результат развития",
                    enabled: mSettings.enabled && mSettings.editable,
                    value: mSettings.devResultText
                });
                oTextArea.addStyleClass("zResultText");
                this.setAggregation("_resTextArea", oTextArea);
   
                var oInput = new Input({
                        placeholder: "Оцените, в %",
                        enabled: true,
                        type: "Number",
                        value: mSettings.procResult
                    }); 
                oInput.addStyleClass("zCTRLprocentInput");
                this.setAggregation("_procInput", oInput);

                oInput.attachEvent("change", this.onPercentInputChange.bind(this));
            }
        },

        onPercentInputChange: function(oEvent) {
            var oParent = oEvent.getSource().getParent();            
            oParent._validatePercent(oEvent);
            oParent._recalcRZL(oEvent);
        },

        _recalcRZL: function(oEvent) {
            var oParent = oEvent.getSource().getParent(),
                compWeight = oParent.getProperty("compWeight") || 0,
                procResult = oParent.getProcResult() || 0, 
                procRZL = 0;
            if (compWeight !== 0 && procResult !== 0) {
                procRZL = compWeight * procResult / 100;
            } 
			oParent.setProperty("procRZL", procRZL);   
            oParent.fireOnPercentChange(oEvent);
        },

        _validatePercent: function(oEvent) {
            var oParent = oEvent.getSource().getParent(),
                nOldValue = parseInt(oEvent.getParameter("value"), 10),
                nValue = nOldValue,
                nPrcHigh = parseInt(oParent.getPrcHigh(), 10) || 0,
                nPrcLow = parseInt(oParent.getPrcLow(), 10) || 0,
                nPrcStep = parseInt(oParent.getPrcStep(), 10) || 0,
                strTooltip = "Введите значение от "+nPrcLow+" до "+nPrcHigh+", или укажите '0', если не выполнено.";

            oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
            oEvent.getSource().setValueStateText("");                    

            if (!isNaN(nValue)) {
                oParent.setProcResultNoValue(false);
                if (nValue < nPrcLow) {
                    nValue = 0;
                }
                if (nValue > nPrcHigh) {
                    nValue = nPrcHigh;
                }
                var nRest = nValue % nPrcStep;
                if (nRest !== 0 || nOldValue !== nValue) {
                    // Округление:
                    nValue = nValue - nValue % nPrcStep;
                    if (nPrcStep / 2 <= nRest) {
                        nValue += nPrcStep;
                    }
                }  
                if (nValue !== nOldValue) {
                    oEvent.getSource().setValueState(sap.ui.core.ValueState.Information);
                    oEvent.getSource().setValueStateText(strTooltip);
                }               
            } else {
                oParent.setProcResultNoValue(true);
            }
            oParent.setProcResult(nValue);  
        },

        getDevResultShort: function () {
            return this.getAggregation("_resSegmBtn").getProperty("selectedKey");
        },
        setDevResultShort: function (value) {
            if (value) {
                this.getAggregation("_resSegmBtn").setSelectedKey(value);
                this.rerender();
            }
        },
        getDevResultText: function () {
            return this.getAggregation("_resTextArea").getProperty("value");
        },
        setDevResultText: function (value) {            
            this.getAggregation("_resTextArea").setValue(value);
            this.rerender();
        },
        getProcResult: function () {
            return this.getAggregation("_procInput").getProperty("value");
        },
        setProcResult: function (value) {   
            if (this.getProcResultNoValue()) {
                value = "";
            } 
            this.getAggregation("_procInput").setValue(value);
            this.rerender();
        },        

        getTileColorStyle: function () {
            var bResultNotSet = true;
            // check short if needed
            if (this.getShortOblig()) {
                if (this.getDevResultShort() && this.getDevResultShort() != "0") {
                    bResultNotSet = false;
                }
            }
            if (this.getDetailedOblig() && bResultNotSet === true) {
                if (this.getDevResultText() && this.getDevResultText() != "") {
                    bResultNotSet = false;
                }
            }
            // if both are not obligatory - we still execute check
            if (!this.getShortOblig() && !this.getDetailedOblig()) {
                if (this.getShortReport()) {
                    if (this.getDevResultShort() && this.getDevResultShort() != "0") {
                        bResultNotSet = false;
                    }
                }
                if (this.getDetailedReport() && bResultNotSet === true) {
                    if (this.getDevResultText() && this.getDevResultText() != "") {
                        bResultNotSet = false;
                    }
                }
            }

            if (bResultNotSet === false) {
                return this.COLOR_CLASSES.Neutral;
            }
            if (bResultNotSet === true) {
                var iDateCompareResult = (this._getUntilDate().getTime() - (new Date()).getTime()) / 1000 / 60 / 60 / 24;
                if (iDateCompareResult < 0) {
                    return this.COLOR_CLASSES.Overdue;
                }
                if (iDateCompareResult > 0 && iDateCompareResult <= 30) {
                    return this.COLOR_CLASSES.WithinMonth;
                }

                return this.COLOR_CLASSES.Neutral;
            }
        },

        _getTodayInYYYYMMDD: function(){
            var today = new Date(),
                yyyy = "" + today.getFullYear(),
                mm = today.getMonth() + 1,
                dd = today.getDate();

            if (mm.toString().length === 1) {
                mm = "0" + mm;
            }
            if (dd.toString().length === 1) {
                dd = "0" + dd;
            }

            return yyyy + mm + dd;
        },

        onBeforeRendering: function () {
            var selector = "",
                oSegBtn = this.getAggregation("_resSegmBtn"),
                oTextArea = this.getAggregation("_resTextArea"),
                bProcResultNoValue = this.getProcResultNoValue(),
                bEnterFirst = this.getHasEnterFirst();
                
            if (!bEnterFirst) {
                this.setHasEnterFirst(true);
                if (bProcResultNoValue) {
                    this.setProcResult("");
                }
            }

            if (oSegBtn) {
                selector = "#"+oSegBtn.sId;
                if ($(selector).hasClass("zErrorBorder")) {
                    this.setHasErrorBorderSegmBtn(true);
                    this.setHasErrorBorder(true);
                }
            }

            if (oTextArea) {
                selector = "#"+oTextArea.sId;
                if ($(selector).hasClass("zErrorBorder")) {
                    this.setHasErrorBorderTextArea(true);
                    this.setHasErrorBorder(true);
                }
            }
        },

        onAfterRendering: function() {
            var oSegBtn = this.getAggregation("_resSegmBtn"),
                oTextArea = this.getAggregation("_resTextArea");

            if (this.getHasErrorBorder()) {
                if (this.getDevResultShort() && this.getDevResultShort() === "0") {
                    oSegBtn.addStyleClass("zErrorBorder");
                } else {
                    oSegBtn.removeStyleClass("zErrorBorder");
                }

                if ( ! this.getDevResultText()) {
                    if (this.getHasErrorBorderTextArea()) {
                        oTextArea.addStyleClass("zErrorBorder");
                    } else {
                        oTextArea.removeStyleClass("zErrorBorder");
                    }
                } else {
                    oTextArea.removeStyleClass("zErrorBorder");
                }
            }
        },

        _getUntilDateStr: function () {
            var d = this.getUntilDate();
            return d.substr(6, 2) + '.' + d.substr(4, 2) + '.' + d.substr(0, 4);
        },
        _getUntilDate: function () {
            var d = this.getUntilDate();
            return new Date(d.substr(0, 4), d.substr(4, 2) - 1, d.substr(6, 2));
        },
        renderer: function (oRM, oControl) {
            var sDate = 'до ' + oControl._getUntilDateStr(),
                sColorClass = oControl.getTileColorStyle.call(oControl),
                sResShort = oControl.getDevResultShort(),
                bShortRep = oControl.getShortReport(),
                bEnabled = oControl.getEnabled(),
                bDetailedRep = oControl.getDetailedReport(),
                bIsPhone = oControl.getIsPhone();
                
            
            oRM.write('<div data-devTileMethRowIID="'+ oControl.getRowIID() +'"');
            oRM.writeControlData(oControl);
            oRM.addClass("zReportTile");
            if (bIsPhone) {
                oRM.addClass("zReportTileMobile");
            }
            oRM.addClass("zDevMethItem");
            if (bEnabled === true) {
                oRM.addClass(sColorClass);
            }
            oRM.writeClasses();
            oRM.write(">");

            oRM.write("<div class='zCRTMajorItem'>");
        		oRM.write(oControl.getCompDescr());
            oRM.write("</div>");

            var sClass = "";
            if (bIsPhone) {
                sClass = " sapUiSmallMarginTopBottom";
            }
            oRM.write("<div class='zCRTItem" + sClass + "'>");
            oRM.write(sDate);
            oRM.write("</div>");

            // new control update 
            if (oControl.getShowCompWeight()) {
                oRM.write('<div class="zCTRLweight">');
                    oRM.write('<span class="zCTRLweightSpan">');
                        oRM.write('Вес: ');oRM.write(oControl.getCompWeight());
                    oRM.write('%</span>');
                oRM.write('</div>');
                
                oRM.write('<div class="zCTRLprocent">');
                    oRM.write('<span>');
                        oRM.write('Процент выполнения:&nbsp;&nbsp;'); //
                        
                        oRM.write('<span>');
                            if (bEnabled === true) {
                                oRM.renderControl(oControl.getAggregation("_procInput"));
                            } else {
                                oRM.write('<span class="zCtrlRzlSpan">');
                                    oRM.write(' ' + oControl.getProcResult());
                                oRM.write('</span>');
                            }
                        oRM.write('</span>');
                    oRM.write('</span>');
                oRM.write('</div>');

                oRM.write('<div class="zCtrlRzl">');
                    oRM.write('<span style="color: #32363a;">'); // white-space: nowrap;&nbsp;
                        oRM.write('Результат выполнения:&nbsp;&nbsp;');

                        oRM.write('<span class="zCtrlRzlSpan">');
                            var n = parseFloat(oControl.getProcRZL()) || 0.0;
                            if (n > 0) {
                                oRM.write( ' ' + Math.round((parseFloat(oControl.getProcRZL()) || 1) * 100) / 100 );
                            } else {
                                oRM.write('<span style="padding-left: 8px;color: #32363a;">');
                                    oRM.write(' ' + String(n));
                                oRM.write('</span>');
                            }
                        oRM.write('</span>');
                    oRM.write('</span>');
                oRM.write('</div>');
            }
            // ------------------
            if (bShortRep || bDetailedRep) {
                sClass = "";
                if (bIsPhone) {
                    sClass = " zCRTResultsContainerMobile";
                }
                oRM.write("<div class='zCRTResultsContainer" + sClass + 
                          "' data-rowIID='" + parseInt(oControl.getRowIID(), 10) + "'>");
            }
            if (bShortRep) {
                if (bEnabled) {
                    var oSegmBtn = oControl.getAggregation("_resSegmBtn");
                    if (bIsPhone) {
                        oSegmBtn.addStyleClass("zResSegmBtnMobile");
                    }

                    if (oControl.getHasErrorBorderSegmBtn()) {
                        if (parseInt(oControl.getDevResultShort(), 10) === 0) {
                            oSegmBtn.addStyleClass("zErrorBorder");
                        }
                    }

                    oRM.renderControl(oSegmBtn);
                } else {
                    switch (sResShort) {
                        case "1":
                            oRM.write("<div class='zBlueTextBold' style='font-size:12px'>Выполнено</div>");
                            break;
                        default:
                            oRM.write("<div style='font-size:12px;font-weight:700;color:rgb(135,135,135)'>Не выполнено</div>");
                            break;
                    }
                }
            }
            if (bDetailedRep) {
                var oTextArea = oControl.getAggregation("_resTextArea");
                oTextArea.addStyleClass("zOpacity1");
                oRM.renderControl(oTextArea);
            }
            if (bShortRep || bDetailedRep) {
                oRM.write("</div>");
            }

            oRM.write("</div>");
        },


    });

    return CompReportTile;
});