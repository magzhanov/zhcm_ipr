sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";

    return Control.extend("zhcm_IPR.customControls.CompetencyPicker", {
        metadata: {
            properties: {
                maxSelect: {type: "string"},
                minSelect: {type: "string"},
                pickedNum: {type: "string"}
            },
            aggregations: {
                items: {type: "zhcm_IPR.customControls.CompetencyTile", multiple: true},
                toPick: {type: "zhcm_IPR.customControls.CompetencyTile", multiple: true},
                alreadyPicked: {type: "zhcm_IPR.customControls.CompetencyTile", multiple: true},

            },
            events: {}
        },

        onBeforeRendering: function () {
            this.setPickedNum(this._countPicked());
        },

        _countPicked: function () {
            var iPicked = 0;
            var aComps = this.getAggregation("items");
            for (var i = 0; i < aComps.length; i++) {
                if (aComps[i].getProperty("picked") == true) {
                    iPicked += 1;
                }
            }
            return iPicked;
        },

        ondrop: function (oEvent) {
            var sTileId = oEvent.originalEvent.dataTransfer.getData("text");
            var oTile = document.getElementById(sTileId);
            var sBin;

            function setPicked(oTile, bPicked) {
                var items = this.getAggregation("items");
                for (var i = 0; i < items.length; i++) {
                    if (items[i].getId() === oTile.id) {
                        items[i].setProperty("picked", bPicked);
                        this.setPickedNum(this._countPicked());
                        return;
                    }
                }
            }

            if (oEvent.target.className.indexOf("zCompContainer") > -1) {
                sBin = oEvent.target;
            } else if (oEvent.target.parentElement.className.indexOf("zCompContainer") > -1) {
                sBin = oEvent.target.parentElement;
            } else if (oEvent.target.parentElement.parentElement.className.indexOf("zCompContainer") > -1) {
                sBin = oEvent.target.parentElement.parentElement;
            }

            sBin.appendChild(oTile);
            switch (sBin.id) {
                case "toPick" :
                    setPicked.call(this, oTile, false);
                    break;
                case "alreadyPicked":
                    setPicked.call(this, oTile, true);
                    break;
                default:
                    break;
            }
        },

        ondragstart: function (oEvent) {
            oEvent.originalEvent.dataTransfer.setData("text", oEvent.target.id);
        },
        ondragover: function (oEvent) {
            oEvent.preventDefault();
        },

        renderer: function (oRM, oControl) {
            var renderTiles = function (aComps, objRM, bPicked) {
                aComps = aComps || [];
                for (var i = 0; i < aComps.length; i++) {
                    if (aComps[i].getProperty("picked") === bPicked) {
                        objRM.renderControl(aComps[i]);
                    }
                }
            };

            var renderCompsContainer = function (objRM, oCtrl, sId, bPicked) {
                objRM.write('<div id=' + sId);
                objRM.addClass("zCompContainer");
                objRM.writeClasses();
                objRM.write(">");

                renderTiles(oCtrl.getAggregation("items"), objRM, bPicked);

                objRM.write("</div>");
            };

            oRM.write("<div");
            oRM.writeControlData(oControl);
            oRM.write(">");

            // left col
            oRM.write("<div");
            oRM.addClass("zDnDWrapper");
            oRM.writeClasses();
            oRM.write(">");
            // Выберите от 1 до 3 компетенций
            var sText;
            if (parseInt(oControl.getMinSelect()) === parseInt(oControl.getMaxSelect())) {
                sText = 'Необходимое количество компетенций для выбора: ' + "<span class='zBlueTextBold'>" +
                    parseInt(oControl.getMinSelect()) + "</span>";
            } else {
                sText = 'Выберите от ' + "<span class='zBlueTextBold'>" +
                    parseInt(oControl.getMinSelect()) + "</span>" + ' до ' + "<span class='zBlueTextBold'>" +
                    parseInt(oControl.getMaxSelect()) + "</span>" + ' компетенций';
            }
            oRM.write("<div style='text-align: Center'>" + sText + "</div>");
            renderCompsContainer(oRM, oControl, "toPick", false);
            oRM.write("</div>");
            // separator
            oRM.write("<div class='zLineSeparator'/>");
            //right col
            oRM.write("<div");
            oRM.addClass("zDnDWrapper");
            oRM.writeClasses();
            oRM.write(">");
            // Выбрано: 1
            sText = 'Выбрано: ' + "<span class='zBlueTextBold'>" + oControl.getPickedNum() + "</span>";
            oRM.write("<div style='text-align: Center'>" + sText + "</div>");
            renderCompsContainer(oRM, oControl, "alreadyPicked", true);
            oRM.write("</div>");

            oRM.write("</div>");
        },

    });
});