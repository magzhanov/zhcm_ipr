/*
    1. Начальный вход
        а. Считать доступные ТНы пользователя
        б. Используя 1ый ТН в списке - читаем данные для вкладок, раскладываем по моделям
    2. Возврат из ИПР
        а. Прочитать только ту вкладку, из которой уходили, ее же и поставить выбранной
*/
sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "zhcm_IPR/controller/iprBase.controller",
    "../utils/Formatter",
    "../utils/Config",
    "../utils/DataAccess",
    "sap/m/Label",
    "sap/m/ColumnListItem",
    "sap/ui/model/Filter"
], function (JSONModel, baseController, Formatter, Config, DataAccess, Label, ColumnListItem, Filter) {
    "use strict";

    return baseController.extend("zhcm_IPR.controller.Overview", {
        formatter: Formatter,

        filterMemo: {
            MyIPR: {},
            MySubords: {},
            TabBeforeNavigation: ""
        },

        DEFAULTS: {
            displayСompletedIPR: true,
            displayСompletedSubords: true,
            fewItemsMyIPR: false,
            fewItemsMySubords: false,
            SubordsTabVisible: false,
            MyIprTabVisible: true,
            MyIprSTabsVisible: false,
            TxtVisible: false,
            NoData: "",
            activeTab: "MyIPR"
        },

        // =======================================================================================================================================================================
        // INIT
        // =======================================================================================================================================================================

        onInit: function () {

            var oComponent = this.getOwnerComponent();
            var oRouter = oComponent.getRouter();

            var viewData = this.DEFAULTS;
            var oViewModel = new JSONModel(viewData);
            this.getView().setModel(oViewModel, "viewData");
            
            var oYears = new JSONModel(this._getYears(2017, new Date().getFullYear()));
            this.getView().setModel(oYears, "Years");

            var mErrorBorder = new JSONModel({
                hasErrorBorder: false,
                Items: []
            });
            this.getView().setModel(mErrorBorder, "mErrorBorder");
            this.getView().setModel(new JSONModel(), "statisticalReport");

            oRouter.getRoute("mOverview").attachMatched(this._onRouteMatched, this);
            oRouter.getRoute("Overview").attachMatched(this._onRouteMatched, this);
            oRouter.navTo(this._getMobilePrefix() + "Overview", { tabId: "MyIPR" });

        },
        
        // =======================================================================================================================================================================
        // ROUTING AND READING
        // =======================================================================================================================================================================

        _onRouteMatched: function (oEvent) {
            var oView = this.getView();
            var oViewModel = oView.getModel("viewData");
            var sTabId = oEvent.getParameter("tabId");
            
            if (!sTabId) {
                sTabId = oViewModel.getProperty("/activeTab"); // default: "MyIPR"
            }
            if (sTabId === "MySubords" && !oViewModel.getProperty("/SubordsTabVisible")) {
                sTabId = "MyIPR";
            }

            if (this._getAppData) {
                // TODO: вместо этого делать navTo при скрытии вкладки после фильтрации
                oView.byId("IconTabBar").setSelectedKey(sTabId); 
            }
            this._updateChipData();
            if (!this._makeInitialReadIfNeeded()) {
                this._allTabsDataRead(false);
            }
        },

        _makeInitialReadIfNeeded: function () {
            var that = this;
            var bInitialReadWasMade = this._getAppData("/initialReadRequired");
            if (bInitialReadWasMade) {
                this._setAppData("/initialReadRequired", false);
                this._getAllUserPernrs().then(function () {
                    that._allTabsDataRead(true);
                });
            }
            return bInitialReadWasMade;
        },

        _getAllUserPernrs: function () {
            var that = this;
            return DataAccess.getUserMemId()
                .then(function (aPernrsMemIdSet) {
                    that._setAppData("/CurrentPernrMemId", aPernrsMemIdSet[0].PERNR_MEMID);
                    that._bindJSONModel("PernrsMemIdSet", aPernrsMemIdSet);
                    that._setCurrentPernr();
                    return aPernrsMemIdSet;
                });
        },

        _allTabsDataRead: function (bUpdateSubordsFilter) {
            var oView = this.getView();
            oView.setBusy(true);
            return $.when(
                    this._getMyIPR(), 
                    this._getTutorShips(), 
                    this._getReportProcSet(), 
                    this._getSubDivs(), 
                    this._getMySubords(true, true, bUpdateSubordsFilter))
                .always(function () { 
                    oView.setBusy(false); 
                });
        },

        _updateChipData: function () {
            var oViewModel = this.getView().getModel("viewData");
            DataAccess.getChipData().then(function (aChipData) {
                var sTasksList = aChipData[0].PERNRS_AM_LIST;
                var aPernrsTasksList = [];
                if (sTasksList) {
                    JSON.parse(aChipData[0].PERNRS_AM_LIST);
                }
                oViewModel.setProperty("/SubordsTasksAmount", aChipData[0].SUBORD_TASKS_AMOUNT);
                oViewModel.setProperty("/SubDivsTasksAmount", aChipData[0].SUBDIVS_TASKS_AMOUNT);
                oViewModel.setProperty("/TutorShipsTasksAmount", aChipData[0].TUTORSHIPS_TASKS_AMOUNT);
                // в PERNRS_AM_LIST приходит строка вида [{"11111111":19 },{"22222222":1 }],
                oViewModel.setProperty("/PernrsAmList", aPernrsTasksList);
            });
        },

        // =======================================================================================================================================================================
        // HANDLERS
        // =======================================================================================================================================================================
        onUploadStatisticalReport: function(){
            if (!this._oStatisticalReportDialog) {
                if (this._getMobilePrefix() === "m") {
                    this._oStatisticalReportDialog = sap.ui.xmlfragment("zhcm_IPR.fragments.mobile.statisticalReportDialog", this);
                } else {
                    this._oStatisticalReportDialog = sap.ui.xmlfragment("zhcm_IPR.fragments.statisticalReportDialog", this);
                }
            }

            var oViewData = this.getView().getModel('viewData');            
            var oStatisticReport = oViewData.getProperty("/StatisticReport");

            if (!oStatisticReport) {
                oViewData.setProperty("/StatisticReport", {});
                oStatisticReport = oViewData.getProperty("/StatisticReport");
            }
            
            if (Object.keys(oStatisticReport).length === 0) {
                oStatisticReport.CbSelProcIPR = '';
                oStatisticReport.CbSelOrgeh = '';
                oStatisticReport.CbSelUser = '';
                oStatisticReport.CbSelType = '';
                oStatisticReport.showErrMess = true;
                this._oStatisticalReportDialog.setModel(oViewData,'viewData');
            }

            this._oStatisticalReportDialog.open();
        },

        onChangeSelProcIPR: function(oEvent){
            var oViewData = this.getView().getModel('viewData');
            var oStatisticReport = oViewData.getProperty("/StatisticReport");
            
            this.getSelOtherData(oStatisticReport.CbSelProcIPR);
            
            if (oStatisticReport.CbSelProcIPR !== '') {
                oStatisticReport.showErrMess = false;
                this.getView().setModel(oViewData,'viewData');
            }
        },

        getSelOtherData: function(sIdDevProc){
            return new Promise(function(resolve, reject){
                $.when(
                    this._getReportOrgehSet(sIdDevProc),
                    this._getReportFioSet(sIdDevProc),
                    this._getReportDevTypeSet(sIdDevProc)
                ).done(function(){
                    resolve();
                })
            }.bind(this))
        },

        selOrgehFinish: function(oEvent){
            // {viewData>/StatisticReport/CbSelOrgeh}
            var oViewData = this.getView().getModel('viewData');            
            var oStatisticReport = oViewData.getProperty("/StatisticReport");
            var selectedItems = oEvent.getParameter("selectedItems");
            selectedItems.forEach(function(item){
                oStatisticReport.CbSelOrgeh += item.getProperty('key') + '_';
            });
            this.getView().setModel(oViewData, 'viewData');
        },
        selUserFinish: function(oEvent){
            // {viewData>/StatisticReport/CbSelUser}
            var oViewData = this.getView().getModel('viewData');            
            var oStatisticReport = oViewData.getProperty("/StatisticReport");
            var selectedItems = oEvent.getParameter("selectedItems");
            selectedItems.forEach(function(item){
                oStatisticReport.CbSelUser += item.getProperty('key') + '_';
            });
            this.getView().setModel(oViewData, 'viewData');
        },
        selTypeFinish: function(oEvent){
            // {viewData>/StatisticReport/CbSelType}
            var oViewData = this.getView().getModel('viewData');            
            var oStatisticReport = oViewData.getProperty("/StatisticReport");
            var selectedItems = oEvent.getParameter("selectedItems");
            selectedItems.forEach(function(item){
                oStatisticReport.CbSelType += item.getProperty('key') + '_';
            });
            this.getView().setModel(oViewData, 'viewData');
        },
        
        confirmUploadStatisticalReport: function(){
            var oViewData = this.getView().getModel('viewData');            
            var oStatisticReport = oViewData.getProperty("/StatisticReport");

            DataAccess.getStatisticalReport(oStatisticReport);

            this.cancelUploadStatisticalReport();
        },

        cancelUploadStatisticalReport: function(){
            this._oStatisticalReportDialog.close();
        },

        onColListItemsTextOpen: function(oEvent){
            var oExpandBtn = oEvent.getSource();
            var oLongTextBox = oExpandBtn.getParent();

            $("#"+oExpandBtn.sId).toggleClass('zBtnRotate');
            $("#"+oLongTextBox.sId+" > .zColListItemsTextLong").toggleClass('zColListItemsTextOpen');
        },

        onIconTabBarSelect: function (oEvent) {
            // construct filter screen
            var sTabId = oEvent.getParameter("key"),
                oFilterContainer = this.byId("FiltersContainer");

            if (oFilterContainer) {
                oFilterContainer.setExpanded(false);
                this.getView().getModel("viewData").setProperty("/fewItems", false);
            }

            this.getView().getModel("viewData").setProperty("/activeTab", sTabId);
            this._setAppData("/activeTab", sTabId); // TODO: оставить только это
            this.getOwnerComponent().getRouter()
                                    .navTo(this._getMobilePrefix() + "Overview", { tabId: sTabId });
        },

        onNavItemPress: function (oEvt) {
            var oSrc = oEvt.getSource(),
                oBindingContext = oSrc.getBindingContext("MyIPR") 
                                || oSrc.getBindingContext("MySubords") 
                                || oSrc.getBindingContext("MySubDivs")
                                || oSrc.getBindingContext("MyTutorShips"),
                oSelectedTask = oBindingContext.getModel().getProperty(oSrc.getBindingContextPath());

            if (oSelectedTask.NEXT_INTERFACE) {
                this.filterMemo.TabBeforeNavigation = this.getView().byId("IconTabBar").getSelectedKey();
                this.onNavigate(this._getMobilePrefix() + oSelectedTask.NEXT_INTERFACE, { 
                    "AppraisalId": oSelectedTask.APPRAISAL_ID,
                    "IndicatorId": oSelectedTask.INDICATOR_ID
                });
            }
        },

        onNavigate: function (sTarget, oParams) {
            this.getOwnerComponent().getRouter().navTo(sTarget, oParams);
        },

        onSubordHelp: function (oEvt) {
            var oInput = oEvt.getSource();
            var sRowsAggregationName, fnItemsFactory;

            if (!this.getView().getModel("MySubords")) { 
                return;
            }

            this.getView().setBusy(true);
            this._oSubordDialog = sap.ui.xmlfragment("zhcm_IPR.fragments.SubordValueHelpDialog", this);
            this.getView().addDependent(this._oSubordDialog);
            this._oSubordDialog.callerInput = oInput;
            
            var oValueHelpCust = Config.valueHelpCust.filter(function (custItem) {
                return oInput.getId().indexOf(custItem.inputId) > -1;
            })[0];
            var aCols = oValueHelpCust.cols;
            var oColModel = new JSONModel({ cols: aCols });

            var oTable = this._oSubordDialog.getTable();
            oTable.setModel(oColModel, "columns");
            oTable.setModel(this.getView().getModel(oValueHelpCust.model));
            
            switch (oTable.getMetadata().getName()) {
                case "sap.ui.table.Table":
                    sRowsAggregationName = "rows";
                    fnItemsFactory = null;
                    break;
                case "sap.m.Table":
                    sRowsAggregationName = "items";
                    fnItemsFactory = function () {
                        return new ColumnListItem({
                            cells: aCols.map(function (column) {
                                return new Label({ text: "{" + column.template + "}" });
                            })
                        });
                    }
                    break;
                default:
                    break;
            }
            oTable.bindAggregation(sRowsAggregationName, "/", fnItemsFactory);

            this._oSubordDialog.setKey(oValueHelpCust.key);
            this._oSubordDialog.setDescriptionKey(oValueHelpCust.descrKey);

            if (oInput.getSelectedKey()) {
                this._oSubordDialog.setTokens([
                    new sap.m.Token({
                        key: oInput.getSelectedKey(),
                        text: oInput.getValue()
                    })
                ]);
            }
            try {
                this._oSubordDialog.update();
                this._oSubordDialog.open();
                this.getView().setBusy(false);
            } catch (error) {
                console.log(error);
                this.getView().setBusy(false);
            }
        },

        onSubordHelpOk: function (oEvent) {
            var aTokens = oEvent.getParameter("tokens");
            this._oSubordDialog.callerInput.setSelectedKey(aTokens[0].getKey());
            this.getView().setBusy(false);
            this._oSubordDialog.close();
        },

        onSubordHelpCancel: function () {
            this.getView().setBusy(false);
            this._oSubordDialog.close();
        },

        onSubordHelpAfterClose: function () {
            this.getView().setBusy(false);
            this._oSubordDialog.destroy();
        },

        onPrintSingle: function (oEvt) {
            var sModelName = this.getView().byId("IconTabBar").getSelectedKey(),
                oCont = oEvt.getSource().getBindingContext(sModelName),
                oDocDataLine = oCont.getModel().getProperty(oCont.getPath());

            DataAccess.getPrintformIPR(oDocDataLine.PERNR, oDocDataLine.ID_DEVPLAN_PROC);
        },

        onPrintMultiple: function (oEvt) {
            var oTab = oEvt.getSource().getParent().getParent(),
                aItems = oTab.getSelectedItems(),
                sModelName = this.getView().byId("IconTabBar").getSelectedKey();

            $.each(aItems, function (sInd, oRowItem) {
                var oCont = oRowItem.getBindingContext(sModelName),
                    oDocDataLine = oCont.getModel().getProperty(oCont.getPath());
                if (oDocDataLine.PERNR && oDocDataLine.ID_DEVPLAN_PROC) {
                    DataAccess.getPrintformIPR(oDocDataLine.PERNR, oDocDataLine.ID_DEVPLAN_PROC);
                }
            });
        },

        onTabSelectionChange: function (oEvt) {
            var sModelName = this.getView().byId("IconTabBar").getSelectedKey(),
                sPropName = "/fewItems" + sModelName;

            if (oEvt.getSource().getSelectedItems().length > 0) {
                this.getView().getModel("viewData").setProperty(sPropName, true);
            } else {
                this.getView().getModel("viewData").setProperty(sPropName, false);
            }
        },

        onTBIconFilterPress: function (oEvt) {
            if (!this._oFiltersDialog) {
                this._oFiltersDialog = sap.ui.xmlfragment("idFiltersDialog", 
                    "zhcm_IPR.fragments.mobile.OverviewFiltersDialog", this);
                this.getView().addDependent(this._oFiltersDialog);
            }
            this._oFiltersDialog.open();
        },

        onFiltersClose: function () {
            this._oFiltersDialog.close();
        },

        onSelectPernrMemIdFilter: function (oEvent) {
            this._getMySubords(false, false, true);
        },
        
        onApplyFilters: function (oEvt) {
            var oOverviewFilters = this._getAppData("/overviewFilters");
            var sPernrMemIdFilter = oOverviewFilters.PernrMemId;
            var oView = this.getView();
            
            // for mobile
            if (this._oFiltersDialog) {
                this._oFiltersDialog.close();
            }

            if (this._getAppData("/CurrentPernrMemId") !== sPernrMemIdFilter) {
                this._setAppData("/CurrentPernrMemId", sPernrMemIdFilter);
                this._setCurrentPernr();
                this._allTabsDataRead(false);
            }

            var oIPRBinding = oView.byId("TableMyIPR").getBinding("items");
            var oSubordsBinding = oView.byId("TableMySubords").getBinding("items");
            var aIPRFilters = [];
            var aSubordsFilters = [];
            var sEQ = sap.ui.model.FilterOperator.EQ;
            var sContains = sap.ui.model.FilterOperator.Contains;
            
            if (oOverviewFilters.ProcStatus) {
                aIPRFilters.push(new Filter("STATUS_EVAL_PROC", sEQ, oOverviewFilters.ProcStatus));
                aSubordsFilters.push(new Filter("STATUS_EVAL_PROC", sEQ, oOverviewFilters.ProcStatus));
            }
            if (oOverviewFilters.Year) {
                aIPRFilters.push(new Filter("PERIOD", sContains, oOverviewFilters.Year));
                aSubordsFilters.push(new Filter("YEAR", sEQ, oOverviewFilters.Year));
            }
            if (oOverviewFilters.Subord) {
                aSubordsFilters.push(new Filter("PERNR", sEQ, oOverviewFilters.Subord));
            }

            oIPRBinding.filter(aIPRFilters);
            oSubordsBinding.filter(aSubordsFilters);
        },

        // =======================================================================================================================================================================
        // UTILITY
        // =======================================================================================================================================================================

        _getYears: function (iStartYear, iCurrYear) {
            var aYears = [""];
            do {
                aYears.push(iStartYear);
                iStartYear += 1;
            }
            while (iStartYear <= iCurrYear + 1);

            return aYears;
        },

        _setCurrentPernr: function () {
            var sCurrentPernrMemId = this._getAppData("/CurrentPernrMemId");
            var aPernrsMemIdSet = this.getView().getModel("PernrsMemIdSet").getData();
            var sPernr = aPernrsMemIdSet.filter(function (oPernr) {
                return oPernr.PERNR_MEMID === sCurrentPernrMemId;
            })[0].DESCR.match(/[0-9]{8}/)[0];

            this._setAppData("/CurrentPernr", sPernr);
        },

       

    });
});