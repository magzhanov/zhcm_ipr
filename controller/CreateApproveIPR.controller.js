sap.ui.define([
    "zhcm_IPR/controller/iprBase.controller",
    "sap/ui/model/json/JSONModel",
    "../utils/DataAccess",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
], function (baseController, JSONModel, DataAccess, MessageBox, MessageToast) {
    "use strict";

    //@ts-check
    return baseController.extend("zhcm_IPR.controller.CreateApproveIPR", {

        onInit: function () {

            this.getOwnerComponent().getRouter().getRoute("CREATE").attachMatched({ instKey: "CREATE" }, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mCREATE").attachMatched({ instKey: "CREATE" }, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("APPROVE").attachMatched({ instKey: "APPROVE" }, this._onRouteMatched, this);            
            this.getOwnerComponent().getRouter().getRoute("mAPPROVE").attachMatched({ instKey: "APPROVE" }, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("ADDAPPROVE").attachMatched({ instKey: "ADDAPPROVE" }, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mADDAPPROVE").attachMatched({ instKey: "ADDAPPROVE" }, this._onRouteMatched, this);

            var oViewModel = new JSONModel({
                HeaderExpanded: true,
                TabCompetenciesEnabled: true,
                TabMethodsEnabled: false,
                FeedInputVisible: false,
                IndicatorId: 1,
                IndicatorFlag: true
            });

            this.getView().setModel(oViewModel, "viewData");

            var oFeed = new JSONModel();
            this.getView().setModel(oFeed, "feedItems");

            var oTopComment = new JSONModel();
            this.getView().setModel(oTopComment, "topComment");

            var oIprModel = new JSONModel();
            this.oView.setModel(oIprModel, "iprData");

            var oDialogModel = new JSONModel();
            this.oView.setModel(oDialogModel, "LibraryDialog");

            var mErrorBorder = new JSONModel({
                hasErrorBorder: false,
                Items: []
            });
            this.getView().setModel(mErrorBorder, "mErrorBorder");  

            var that = this,
                binding = new sap.ui.model.Binding(oIprModel, "iprData>DevMethods", oIprModel.getContext("/"));
            binding.attachChange(function(oEvt) {
                that._changeUICtrlValue(oEvt, that);
            });
        },

        _changeUICtrlValue: function(oEvt, that) {
            if (that._addingToType) {
                if (oEvt.getId() === "change") {
                    var sPathType = that._addingToType,
                        aParts = sPathType.split("/");
                    if (aParts.length >= 4) {
                        var oModel = that.getView().getModel("iprData"),
                            oDevType = oModel.getProperty(sPathType),
                            selectorDevTypeUICtrl = '[data-devTypeId="' + oDevType.TYPE_ID + '"]',
                            sCompPath = "/" + aParts[1] + "/" + aParts[2],
                            oCompetency = oModel.getProperty(sCompPath),
                            selectorCompUICtrl = '[data-compid="' + oCompetency.COMP_ID + '"]',
                            oCompUICtrl = $(selectorCompUICtrl),
                            oDevTypeUICtrl = $(selectorDevTypeUICtrl, oCompUICtrl);

                        if (oDevType.DevMethods ) {
                            oCompUICtrl.removeClass("zErrorBorder");
                            oDevTypeUICtrl.removeClass("zErrorBorder");
                        }
                    }
                }
            }
        },

        _getFeedIfNeeded: function (sAppraisalId, sVariant) {
            // создание\доработка: в верхней части - только первый комментарий
            // на согласовании - все комменты внизу
            var oFeedList = this.getView().byId("idFeedList"),
                oViewDataModel = this.getView().getModel('viewData'),
                viewData = oViewDataModel.getData(),
                that = this;

            if (oFeedList) {
                viewData.FeedListBusy = true;
                viewData.feedItemsCount = 0;
                oViewDataModel.setData(viewData);

                DataAccess.getFeed(sAppraisalId)
                    .then(
                        function (oFeedData) {
                            if (oFeedData.length > 0) {
                                if (sVariant === "CREATE") {
                                    var aCommentsWithoutTop = $.extend([], oFeedData),
                                        aTopComment = [];

                                    aTopComment.push(oFeedData[0]);
                                    aCommentsWithoutTop.shift();
                                    that.getView().getModel("topComment").setData(aTopComment);
                                    that._refreshFeedData(that, aCommentsWithoutTop);
                                    viewData.feedItemsCount = aCommentsWithoutTop.length;
                                } else {
                                    that._refreshFeedData(that, oFeedData);
                                    viewData.feedItemsCount = oFeedData.length;
                                }
                            }

                            // убираем идикатор ожидания.
                            viewData.FeedListBusy = false;
                            oViewDataModel.setData(viewData);
                        },
                        function (oError) {
                            // убираем идикатор ожидания.
                            viewData.FeedListBusy = false;
                            oViewDataModel.setData(viewData);
                        }
                    );
            }
        },

        handleAddFromLibrary: function (oEvt) {
            var oLibData = this.getView().getModel("LibraryDialog").getData(),
                oDevType = this.getView().getModel("iprData").getProperty(this._addingToType),
                aUntil,
                sDescr;

            // #FIX обрабатываем случай, когда пользователь вводит дату с клавиатуры в формате, 
            // отличном от yyyyMMdd - так не работает. Надо поменять числа местами
            // oLibData.UNTIL = oLibData.UNTIL.match(/\d/g).join("");

            if (/^\d\d\.\d\d\.\d\d\d\d$/.test(oLibData.UNTIL)) {
                aUntil = oLibData.UNTIL.split(".");
                oLibData.UNTIL = aUntil[2] + aUntil[1] + aUntil[0];
            }

            if (oLibData.MANUAL) {
                sDescr = oLibData.MANUAL;
                oLibData.SelectedKey = "99999999";

            } else if (oLibData.SelectedKey && oLibData.SelectedKey !== "00000000") {
                sDescr = oLibData.Methods.filter(function (element) {
                    if (element.ID_DEV_SORT === oLibData.SelectedKey) {
                        return true;
                    }
                });
                sDescr = sDescr[0].DESCR_DEV_SORT;
            }

            if (!sDescr) {
                MessageToast.show("Не указан метод развития");
                return;
            }

            sDescr = sDescr.replace(/[\r\n]/g, " ")
                           .replace(/[\n\r]/g, " ")
                           .replace(/[\r]/g, " ")
                           .replace(/[\n]/g, " ");

            var oNewMethod = {
                METH_ID: oLibData.SelectedKey,
                METH_NAME: sDescr,
                UNTIL: oLibData.UNTIL
            };

            //validation - is already added?
            if (oNewMethod.METH_ID !== "99999999") {
                var oSameMethod = oDevType.DevMethods.filter(
                    function (item) {
                        return item.METH_ID === oNewMethod.METH_ID;
                    });
                if (oSameMethod.length > 0) {
                    MessageToast.show("Этот способ развития уже в списке");
                    return;
                }
            }

            oDevType.DevMethods.push(oNewMethod);
            oDevType._expanded = true;

            this.getView().getModel("iprData").refresh(true);
            this._oLibraryDialog.close();
        },

        onPickMethodsClick: function (oEvt) {
            var sTypeId = oEvt.getSource().data().typeId;
            // go from button to panel to get competencyData
            var oPanel = oEvt.getSource().getParent().getParent(),
                oContext = oPanel.getBindingContext("iprData"),
                bAddLevel = oContext.getModel("iprData").getData().ADD_LEVEL,                
                oDevType = oContext.getModel().getProperty(oContext.getPath()),
                sLibId = oContext.getModel().getProperty("/ID_LIBRARY"),
                sCompPath = oContext.getPath().split("/DevTypes")[0],
                oCompetency = oContext.getModel().getProperty(sCompPath),
                nLevel = oCompetency.LEVEL || 0,
                that = this;

            if (bAddLevel) {
                if (nLevel === 0) {
                    MessageBox.error("Предварительно необходимо установить 'Уровень развития компетенции'");
                    return;
                }
            }

            this._addingToType = oContext.getPath();

            DataAccess.getMethodsForComp(sTypeId, sLibId, oCompetency.COMP_ID, oCompetency.LEVEL)
                .then(
                    function (oData) {
                        var oDialogData = {
                                Title: oDevType.TYPE_NAME,
                                MethodsDropVisible: true,
                                FreeInputLabel: "Или введите свое предложение по развитию:",
                                Methods: oData
                            };
                        if (oData.length === 1 && oData[0].ID_DEV_SORT === "00000000") {
                            oDialogData = {
                                Title: oDevType.TYPE_NAME,
                                MethodsDropVisible: false,
                                FreeInputLabel: "Введите свое предложение по развитию:",
                                Methods: null
                            };
                        }
                        that.getView().getModel("LibraryDialog").setData(oDialogData);
                        that._showLibraryDialog();
                    },
                    function (oError) {}
                );
        },

        _showLibraryDialog: function () {
            if (!this._oLibraryDialog) {
                this._oLibraryDialog = sap.ui.xmlfragment("libDialog", "zhcm_IPR.fragments.LibraryDialog", this);
                this.getView().addDependent(this._oLibraryDialog);

                // чиним баг с некорректным позиционированием диалога выбора даты при первом вызове
                // для этого вызываем методы создания календаря сразу
                var oDatePickerUntil = sap.ui.core.Fragment.byId("libDialog", "idDatePickerUntil");
                oDatePickerUntil._createPopup.call(oDatePickerUntil);
                oDatePickerUntil._createPopupContent.call(oDatePickerUntil);
            }
            this._oLibraryDialog.open();
        },

        handleLibraryClose: function () {
            this._oLibraryDialog.close();
        },
        
        // Простая валидация
        // #TODO переделать на кастомный тип данных
        onChangeDatePickerUntil: function (oEvent) {
            var sValue = oEvent.getParameter("value"),
                oSource = oEvent.getSource(),                
                oLibraryDialog = this.getView().getModel("LibraryDialog");

            if (!/^\d\d\d\d\d\d\d\d$/.test(sValue) && !/^\d\d\.\d\d\.\d\d\d\d$/.test(sValue)) {
                oSource.setValueState("Error");
                oLibraryDialog.setProperty("/UNTIL_IS_INVALID", true);
            } else {
                var bValid = oEvent.getParameter("valid"),
                    parseDate = Date.parse(sValue);
                    
                if (bValid || !isNaN(parseDate)) {
                    oSource.setValueState("None");
                    oLibraryDialog.setProperty("/UNTIL_IS_INVALID", false);               
                } else {
                    oSource.setValueState("Error");
                    oLibraryDialog.setProperty("/UNTIL_IS_INVALID", true);
                }
            }
        },

        _isApproveView: function () {
            return !!~this.getView().getViewName().indexOf("Approve");
        },

        constructView: function (oIprData) {
            var bApprove = this._isApproveView();
            if (bApprove) { // it's Approve view
                this.onAddMethods();
            } else { // it's create view
                var nMin = parseInt(oIprData.COMP_MIN, 10);
                var nCounter = this._getPickedCompNum(oIprData.Competencies);
                this.getView().getModel("viewData").setProperty("/PickedNum", nCounter);
                if (nMin <= nCounter) { 
                    this.onAddMethods();
                 } else { 
                    this.onToCompsSelect();
                 }
            }
        },

        onMCreateCompetenciesSelectionChange: function (oEvent) {
            var oViewModel = this.getView().getModel("viewData");
            oViewModel.setProperty("/PickedNum", this._getPickedCompNum());
        },

        _getPickedCompNum: function (aComp) {
            var aComp2 = aComp;
            if (!aComp) {
                aComp2 = this.getView().getModel("iprData").getProperty("/Competencies");
            } 
            return aComp2.filter(function (oComp) {
                return oComp.PICKED;
            }).length;
        },

        onAddMethods: function () {
            var oView = this.getView(),
                bApprove = this._isApproveView();
            oView.getModel("viewData").setProperty("/TabMethodsEnabled", true);
            oView.getModel("viewData").setProperty("/TabCompetenciesEnabled", false);
            // Ждем, пока отработает биндинг, и включатся табы - иначе выбор таба не отработает
            $.sap.delayedCall(0, null, function () {
                var strPageName = "approve";
                if (!bApprove) {
                    strPageName = "create";
                }
                oView.byId(strPageName + "IprTabBar").setSelectedKey("tabMethods");
            });
        },

        onToCompsSelect: function () {
            var oView = this.getView(),
                bApprove = this._isApproveView();
            this.getView().getModel("viewData").setProperty("/TabMethodsEnabled", false);
            this.getView().getModel("viewData").setProperty("/TabCompetenciesEnabled", true);
            // Ждем, пока отработает биндинг, и включатся табы - иначе выбор таба не отработает
            $.sap.delayedCall(0, null, function () {
                var strPageName = "approve";
                if (!bApprove) {
                    strPageName = "create";
                }
                oView.byId(strPageName + "IprTabBar").setSelectedKey("tabCompetencies");
            });
        },

        onDeleteMethod: function (oEvt) {
            var that = this,
                oContainer = oEvt.getSource().getParent(),
                aDialogContent = [],
                sFullText = "Вы уверены, что хотите удалить метод?";

            aDialogContent.push(new sap.m.FormattedText({htmlText: sFullText}));

            var oCancelBtn = new sap.m.Button({
                text: "Отменить",
                type: "Reject",
                press: function () {
                    dialog.close();
                }
            });

            var dialog = new sap.m.Dialog({
                title: "Подтверждение",
                icon: "sap-icon://question-mark",
                type: "Message",
                content: aDialogContent,
                initialFocus: oCancelBtn,
                beginButton: new sap.m.Button({
                    text: "Удалить",
                    type: "Emphasized",
                    press: function () {
                        dialog.close();
                        that._deleteMethod(oContainer);
                    }
                }),
                endButton: oCancelBtn,
                afterClose: function () {
                    dialog.destroy();
                }
            }).addStyleClass("ZHCM_PM_0681 zCustomMessageBox");

            dialog.open();
        },

        _deleteMethod: function (oContainer) {
            //берем информацию о том, какой метод в какой компетенции удаляется
            //c биндинга Text, который в одном контейнере с кнопкой
            var oNeighbourText = oContainer.getItems()[0],
                sFullPath = oNeighbourText.getBindingContext("iprData").getPath();

            var aParts = sFullPath.split("/"),
                oIprModel = this.getView().getModel("iprData"),
                aDevMethods = oIprModel.getData().Competencies[aParts[2]].DevTypes[aParts[4]].DevMethods;
            aDevMethods.splice(aParts[6], 1);

            oIprModel.refresh(true);
        },

        onReturn: function (oEvt) {
            this._inquiryEnterCommentForEmployee("ZREJECTIPR");
        },

        onSaveAppraisal: function (oEvt) {
            this._processSend("");
        },

        onApprove: function (oEvt) {
            var oIprData = this.oView.getModel("iprData").getData(),
                sCommand = "ZAPPROVEIPR"; 
            if (oIprData.MORE_APPROVALS) {
                sCommand = "ZAPPROVEIPR_N";
            }
            this._processSend(sCommand);
        },

        onAddApprove: function (oEvt) {
            var oIprData = this.oView.getModel("iprData").getData(),
                nStatus = parseInt(oIprData.STATUS_EVAL_PROC, 10) || 0,
                sSubStatus = oIprData.STATUS_SUB_EVAL_PROC,
                sCommand = ""; 
            if (nStatus === 2) {
                if (sSubStatus === "L") {
                    sCommand = "ZAPPROVEIPR_N";
                } else if (sSubStatus === "M") {
                    sCommand = "ZAPPROVEIPR";
                } else {
                    MessageBox.error("Некорректная ситуация: документ не на том подстатусе");
                    return;
                }
            } else {
                MessageBox.error("Некорректная ситуация: документ не на том статусе");
                return;
            }
            this._processSend(sCommand);
        }, 
               
        onSendApprove: function (oEvt) {
            this._processSend("ZSENDTOAPPRIPR");
        },

        onSendNoApprove: function (oEvt) {
            this._processSend("ZCONFIRMIPR");
        },

        onChangelevels: function(oEvent){
            var sNewValue = oEvent.getParameter("selectedItem").getKey() || oEvent.getSource().getSelectedKey(),
                sCompId = oEvent.getSource().getCustomData()[0].getValue(),
                oCompetencies = this.getView().getModel('iprData').getData().Competencies,
                oLevelsData = this.oView.getModel('levels').getData(),
                oCompetence = null;
        
            oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
            oEvent.getSource().setValueStateText("");
        
            if (oLevelsData && oCompetencies) {
                for (var i = 0; i < oCompetencies.length; i++) {
                    if (oCompetencies[i].COMP_ID === sCompId) {
                        oCompetence = oCompetencies[i];
                        break;
                    }
                }
        
                if (oCompetence) {
                    if (oCompetence.NO_ROLE_MODEL === true) {
                        if (oLevelsData[oLevelsData.length-1].LEVEL === sNewValue) {
                            oEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
                            oEvent.getSource().setValueStateText("Выбор данного уровня развития недоступен.");
                            return;
                        }
                    }
        
                    for (var j = 0; j < oLevelsData.length; j++) {
                        if (oLevelsData[j].LEVEL === sNewValue) {
                            oCompetence.LEVEL = Number(oLevelsData[j].id);
                            return;
                        }
                    }
                }
            }                                     
        },

        onWeightChange: function(oEvent) {
            var oInput = oEvent.getSource(),
                nOldValue = parseInt(oEvent.getParameter("value"), 10) || 0,
                nValue = nOldValue,
                oIprData = this.oView.getModel("iprData").getData(),
                nWeightHigh = parseInt(oIprData.WEIGHT_HIGH, 10) || 0,
                nWeightLow = parseInt(oIprData.WEIGHT_LOW, 10) || 0,
                nWeightStep = parseInt(oIprData.WEIGHT_STEP, 10) || 0;

            if (nValue < nWeightLow) {
                nValue = nWeightLow;
            }
            if (nValue > nWeightHigh) {
                nValue = nWeightHigh;
            }
            var nRest = nValue % nWeightStep;
            if (nRest !== 0 || nOldValue !== nValue) {
                // Округление:
                nValue = nValue - nValue % nWeightStep;
                if (nWeightStep / 2 <= nRest) {
                    nValue += nWeightStep;
                }
                oInput.setValue(nValue);  
            }       
        },


    });
});