sap.ui.define([
    "zhcm_IPR/controller/iprBase.controller",
    "sap/ui/model/json/JSONModel",
    "../utils/DataAccess",
    "sap/m/MessageBox",
], function (baseController, JSONModel, DataAccess, MessageBox) {
    "use strict";

    return baseController.extend("zhcm_IPR.controller.ReportIPR", {

        onInit: function () {

            // В общем... чтоб не плодить вьюхи он пытается по роуту запустить одну из строк. Какая верная та и заработает.
            this.getOwnerComponent().getRouter().getRoute("FEEDBACK").attachMatched({instKey: "FEEDBACK"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mFEEDBACK").attachMatched({instKey: "FEEDBACK"}, this._onRouteMatched, this);            
            this.getOwnerComponent().getRouter().getRoute("REPORT").attachMatched({instKey: "REPORT"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mREPORT").attachMatched({instKey: "REPORT"}, this._onRouteMatched, this);            
            this.getOwnerComponent().getRouter().getRoute("ADDREPAPP").attachMatched({instKey: "ADDREPAPP"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mADDREPAPP").attachMatched({instKey: "ADDREPAPP"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("REPAPPROVE").attachMatched({instKey: "REPAPPROVE"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mREPAPPROVE").attachMatched({instKey: "REPAPPROVE"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("REPCORRECT").attachMatched({instKey: "REPCORRECT"}, this._onRouteMatched, this);
            this.getOwnerComponent().getRouter().getRoute("mREPCORRECT").attachMatched({instKey: "REPCORRECT"}, this._onRouteMatched, this);
            
            var oViewModel = new JSONModel({
                HeaderExpanded: true,
                FeedInputVisible: true,
                FeedListBusy: false,
                feedItemsCount: 0,
                IndicatorId: 1,
                IndicatorFlag: true
            });
            this.getView().setModel(oViewModel, "viewData");

            var oFeed = new JSONModel();
            this.getView().setModel(oFeed, "feedItems");

            var oTopComment = new JSONModel();
            this.getView().setModel(oTopComment, "topComment");            

            var oIprModel = new JSONModel();
            this.oView.setModel(oIprModel, "iprData");

            this.oView.setModel(new JSONModel(), "levels");
            
            var mErrorBorder = new JSONModel({
                hasErrorBorder: false,
                Items: []
            });
            this.getView().setModel(mErrorBorder, "mErrorBorder");
        },

        _getFeedIfNeeded: function(sAppraisalId, sVariant) {
            var that = this;
            var oViewDataModel = this.getView().getModel('viewData');
            var viewData = this.getView().getModel('viewData').getData();

            viewData.FeedListBusy = true;
            viewData.feedItemsCount = 0;
            oViewDataModel.setData(viewData);

            DataAccess.getFeed(sAppraisalId)
                .then(
                    function (oFeedData) {
                        if (oFeedData.length > 0) {
                            if (sVariant === "REPCORRECT") {
                                var aCommentsWithoutTop = $.extend([], oFeedData),
                                aTopComment = [];

                                aTopComment.push(oFeedData[0]);
                                aCommentsWithoutTop.shift();
                                that.getView().getModel("topComment").setData(aTopComment);
                                that._refreshFeedData(that, aCommentsWithoutTop);
                                viewData.feedItemsCount = aCommentsWithoutTop.length;
                            } else {
                                that._refreshFeedData(that, oFeedData);
                                viewData.feedItemsCount = oFeedData.length;
                            }
                        }
                        
                        // убираем идикатор ожидания.
                        viewData.FeedListBusy = false;
                        oViewDataModel.setData(viewData);
                    },
                    function (oError) {
                        viewData.FeedListBusy = false;
                        oViewDataModel.setData(viewData);
                    }
                );            
        },

        onChangelevels: function(oEvent){
            // НУЖНО
        },

        constructView: function(oIprData){
            // НУЖНО
        },

        onSaveAppraisal: function (oEvt) {
            this._processSend("");
        },

        onCompleteReport: function (oEvt) {
            this._processSend("ZENDIPR");
        },

        handleOverallTextAreaChange: function (oEvent) {
            var bFlag = false,
                sValue = oEvent.getParameter("value"),
                mErrorBorder = this.getView().getModel("mErrorBorder"),
                selector = "#" + oEvent.getSource().getId(),
                oUICtrl = $(selector);

            if (sValue && sValue.length > 0) {
                bFlag = $(selector).hasClass("zErrorBorder");                    
                if (bFlag) {
                    mErrorBorder.setProperty("/hasErrorBorder", true);
                    if (oUICtrl) {                        
                        oUICtrl.removeClass("zErrorBorder");
                    }                 
                }
            } else {
                bFlag = mErrorBorder.getProperty("/hasErrorBorder");
                if (bFlag && oUICtrl) {
                    oUICtrl.addClass("zErrorBorder");
                    this._addItemErrorBorder(mErrorBorder, $(oUICtrl).prop("id"));
                }                
            }
        },

        onPercentChange: function(oEvent) {
            var iprData = this.getView().getModel('iprData').getData(),                
                oCompetencies = iprData.Competencies,
                oSource = oEvent.getSource(),
                sPrc = oSource.getProcResult(),
                sRzl = oSource.getProcRZL(),
                methId = oSource.getMethID(),
                rowIID = oSource.getRowIID(),
                compDescr = oSource.getCompDescr(); 
                         
            // Расчёт 'Результат выполнения' (RZL) для компетенции
            var m = 100,
                n = 0,                
                procRZL = 0,
                nCountMethods = 0,
                bFlag = false;
            for (var i = 0; i < oCompetencies.length; i ++) {
                var oCompetence = oCompetencies[i];
                for (var j = 0; j < oCompetence.DevTypes.length; j ++) {
                    var oDevType = oCompetence.DevTypes[j];
                    for (var k = 0; k < oDevType.DevMethods.length; k ++) {
                        var oDevMethod = oDevType.DevMethods[k];
                        if (oDevMethod.METH_ID === methId && 
                            oDevMethod.ROW_IID === rowIID && 
                            oDevMethod.METH_NAME === compDescr) {

                            if (oDevMethod.PRC !== sPrc) {
                                oDevMethod.PRC = sPrc;
                            }
                            if (oDevMethod.RZL !== sRzl) {
                                oDevMethod.RZL = sRzl;
                            }
                            
                            for (var g = 0; g < oCompetence.DevTypes.length; g ++) {
                                var oDevType2 = oCompetence.DevTypes[g],
                                    procResult = -1;
                                nCountMethods = 0;                                
                                for (var f = 0; f < oDevType2.DevMethods.length; f ++) {
                                    procRZL += Number(oDevType2.DevMethods[f].RZL);

                                    if (isNaN(parseFloat(oDevType2.DevMethods[f].PRC, 10)) === false) {
                                        oDevType2.DevMethods[f].PRC_NO_VALUE = false;
                                        if (procResult === -1) {
                                            procResult = 0;
                                        }     
                                        n = Number(oDevType2.DevMethods[f].PRC);                                   
                                        procResult += n;                                       
                                    } else {
                                        oDevType2.DevMethods[f].PRC_NO_VALUE = true;
                                    }
                                    nCountMethods ++;
                                }

                                if (procResult > -1) {
                                    n = Math.round(procResult / nCountMethods * m) / m;
                                    oCompetence.PRC = String(n);
                                    oCompetence.PRC_NO_VALUE = false;
                                } else {
                                    n = parseFloat(oCompetence.PRC, 10) || -1;
                                    if (n <= 0) {
                                        oCompetence.PRC_NO_VALUE = true;
                                    }
                                }
                            }

                            // округлить до 2 десятичных знаков
                            n = 0;
                            if (procRZL !== 0) {
                                n = Math.round(procRZL * m) / m;
                            }
                            oCompetence.RZL = String(n);  // Только для текущей компетенции                         

                            bFlag = true;
                            break;
                        }
                        if (bFlag) {
                            break;
                        }
                    }
                    if (bFlag) {
                        break;
                    }
                }
                if (bFlag) {
                    break;
                }
            }

            // Расчёт 'Результат выполнения' (RZL) для всего документа
            n = procRZL = 0;
            for (var i = 0; i < oCompetencies.length; i ++) {
                var oCompetence = oCompetencies[i];
                for (var j = 0; j < oCompetence.DevTypes.length; j ++) {
                    var oDevType = oCompetence.DevTypes[j];
                    for (var k = 0; k < oDevType.DevMethods.length; k ++) {
                        procRZL += Number(oDevType.DevMethods[k].RZL);
                    }
                }
            }

            // округлить до 2 десятичных знаков
            if (procRZL !== 0) {
                n = Math.round(procRZL * m) / m;
            }
            iprData.RZL = String(n);
        },

        onReturnToEditingIPR: function(oEvent) {
        	this._showConfirmDialog();
        },

        onSendApprove: function(oEvent) {
            this._processSend("ZENDIPR_SSC");
        },

        onReturn: function(oEvent) {
            this._inquiryEnterCommentForEmployee("ZSENDBACKIPR_SS");    
        },

        onSendRepApprove: function(oEvent) {
            var oIprData = this.oView.getModel("iprData").getData(),
                sCommand = "ZAPPROVEIPR_SSC"; 
            if (oIprData.MORE_APPROVALS) {
                sCommand = "ZAPPROVEIPR_REP";
            }
            this._processSend(sCommand);
        },

        onAddRepSendApprove: function(oEvent) {
            var oIprData = this.oView.getModel("iprData").getData(),
                nStatus = parseInt(oIprData.STATUS_EVAL_PROC, 10) || 0,
                sSubStatus = oIprData.STATUS_SUB_EVAL_PROC; 
            if (nStatus === 4) {
                if (sSubStatus === "Y") {
                    this._processSend("ZAPPROVEIPR_REP");
                    return;
                } else if (sSubStatus === "N") {
                    this._processSend("ZAPPROVEIPR_SSC");
                    return;
                }
            }
            MessageBox.error("Некорректная ситуация: документ не на том статусе");
        },

        _showConfirmDialog: function () {
            if (!this._oConfirmDialog) {
                this._oConfirmDialog = sap.ui.xmlfragment("ConfirmDialog", "zhcm_IPR.fragments.ConfirmDialog", this);
                this.getView().addDependent(this._oConfirmDialog);
            }
            this._oConfirmDialog.open();
        },

        handleConfirmClose: function () {
            this._oConfirmDialog.close();
        },
        
        handleConfirmOK: function (oEvt) {
        	this._processSend("ZSENDTOCREATE");
        },

    });
});