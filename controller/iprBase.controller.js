                            
sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "../utils/DataAccess",
    "../utils/Formatter",
    "sap/m/MessageBox",
    "sap/m/MessagePopover",
    "sap/m/MessagePopoverItem"
], function (JSONModel, Controller, DataAccess, Formatter, MessageBox, MessagePopover, MessagePopoverItem) {
    "use strict";

    //@ts-check
    var oMessagePopover = new MessagePopover({
        items: {
            path: "/",
            template: new MessagePopoverItem({
                type: "{type}",
                title: "{title}",
                activeTitle: "{activeTitle}",
                enabled: "{enabled}",
                subtitle: "{subtitle}",
                description: "{description}",
                active: "{active}",
                group: "{group}"
            })
        }
    });

    oMessagePopover.addStyleClass('zMessagePopoverPress').addStyleClass("ZHCM_PM_0681");


    return Controller.extend("zhcm_IPR.controller.iprBase", {
        formatter: Formatter,

        _onRouteMatched: function (oEvt, mParams) {
            var oArgs = oEvt.getParameter("arguments"),
                that = this,
                oViewData = this.getView().getModel("viewData"),
                sPenrMemId = this._getAppData("/CurrentPernrMemId"),
                oIprDataDef;

            oViewData.setProperty("/InstKey", mParams.instKey);
            oViewData.setProperty("/IndicatorId", oArgs.IndicatorId);
            if (oArgs.IndicatorId < 2) {
                oViewData.setProperty("/IndicatorFlag", true);
            } else {
                oViewData.setProperty("/IndicatorFlag", false);
            }
            
            this._clearDataMessagePopover();
            this.getView().setBusy(true);

            if (sPenrMemId) {
                oIprDataDef = this.getAndParseIprData(oArgs.AppraisalId, sPenrMemId);
            } else {
                oIprDataDef = new $.Deferred();
                DataAccess.getUserMemId()
                    .then(
                        function (oMemIds) {
                            that._setAppData("/CurrentPernrMemId", oMemIds[0].PERNR_MEMID);
                            that.getAndParseIprData.call(that, oArgs.AppraisalId, oMemIds[0].PERNR_MEMID)
                                .then( function (oIprData) { 
                                    oIprDataDef.resolve(oIprData); 
                                });
                        },
                        function (oError) {
                            console.log("Was not able to acquire PERNR_MEMID");
                        }
                    );
            }

            this._getFeedIfNeeded(oArgs.AppraisalId, mParams.instKey);

            oIprDataDef.then(function (oIprData) {
                oViewData.setProperty(
                    "/InstructionButton",
                    DataAccess.getInstrText.call(that, 
                        mParams.instKey, "BTN_POPUP", oIprData.ID_DEVPLAN_PROC)
                );
                if (oIprData.LOCKED) {
                    oViewData.setProperty("/IndicatorFlag", false);
                    sap.m.MessageBox.warning(oIprData.LOCK_MSG);
                } 
            });
        },

        getAndParseIprData: function (sAppraisalId, sPernrMemId) {
            var that = this;
            var oIprDataDef = DataAccess.getIprData(sAppraisalId, sPernrMemId);

            oIprDataDef.then(
                function (oData) {
                    that._parseIPR.call(that, oData, sPernrMemId);
                    that.constructView.call(that, oData);
                    that.getView().setBusy(false);
                },
                function (oError) {
                    that.getView().setBusy(false);
                    console.log("IPR data read error occured");
                }
            );

            return oIprDataDef;
        },

        _parseIPR: function (oData, sPernrMemId) {
            var mIprData = oData,
                oViewDataModel = this.getView().getModel("viewData"),
                sInstKey = oViewDataModel.getProperty("/InstKey"),
                sHdrInstructionText = DataAccess.getInstrText.call(this, 
                    sInstKey, "HEADER", mIprData.ID_DEVPLAN_PROC);

            this.getView().setModel(new JSONModel([
                {
                    id: 1,
                    LEVEL: "Базовый"
                },
                {
                    id: 2,
                    LEVEL: "Устойчиво демонстрируемый"
                },
                {
                    id: 3,
                    LEVEL: "Ролевая модель"
                }
            ]), "levels");

            oViewDataModel.setProperty("/AddInfoText", sHdrInstructionText);
            mIprData.Competencies = oData.Competencies.results || oData.Competencies;
            mIprData.PERNR_MEMID = sPernrMemId;
            mIprData.APPROVE_INVERT = true;

            if ((mIprData.DOC_ROLE_ID === "ZA" || mIprData.DOC_ROLE_ID === "ZM") && 
                (oViewDataModel.getProperty("/IndicatorFlag") === true)) {
                mIprData.VIEW_ENABLED = true;
            }
            if (this.getView().getViewName().indexOf("RepApprove") >= 0 ||
                this.getView().getViewName().indexOf("AddRepApp") >= 0 ||
                this.getView().getViewName().indexOf("RepCorrect") >= 0) {
                mIprData.FEED_VISIBLE = false;
            }

            $.each(mIprData.Competencies, function(indC, oComp) {
                oComp.DevTypes = oComp.DevTypes.results || oComp.DevTypes;
                oComp._contentVisible = true;
                oComp._compVisible = false;
                $.each(oComp.DevTypes, function(indT, oDevType) {
                    oDevType.DevMethods = oDevType.DevMethods.results || oDevType.DevMethods;
                    oDevType._expanded = false;
                    if (oDevType.DevMethods.length > 0) {
                        oDevType._expanded = true;
                    }
                    if(oDevType.DevMethods.length > 0){
                        oComp._compVisible = true; // Вся компетенция видна, если есть что-то внутри

                        // Заменяем в тексте METH_NAME 'переносы на новую строку' на 'пробел'
                        $.each(oDevType.DevMethods, function(sIndM, oDevMethod){
                            oDevMethod.METH_NAME = oDevMethod.METH_NAME
                                                        .replace(/[\r\n]/g, " ")
                                                        .replace(/[\n\r]/g, " ")
                                                        .replace(/[\r]/g, " ")
                                                        .replace(/[\n]/g, " ");
                        });

                        // Изменяем порядок отрисовки
                        oDevType.DevMethods.sort(function(a, b) {
                          if (b == null) {
                                return -1;
                            }
                            if (a == null) {
                                return 1;
                            }

                            var aa = parseInt(a.SHORT_RES, 10);
                            if (aa === 2) {
                                aa = -1;
                            }
                            var bb = parseInt(b.SHORT_RES, 10);
                            if (bb === 2) {
                                bb = -1;
                            }

                            if (aa === 1 || bb === 1){
                                if (aa < bb) {
                                    return -1;
                                }
                                if (aa > bb) {
                                    return 1;
                                }
                            }
                            if (aa.DETAILED_RES !== null && 
                                aa.DETAILED_RES !== undefined && 
                                aa.DETAILED_RES.length > 0) {
                                return 1;
                            }
                            if (bb.DETAILED_RES !== null && 
                                bb.DETAILED_RES !== undefined && 
                                bb.DETAILED_RES.length > 0) {
                                return 1;
                            }
                            return 0;
                        });
                    }
                });
            });

            if (mIprData.STAT_FEEDBACK) {
                oViewDataModel.setProperty("/ViewActionsEnabled", false);
            } else {
                oViewDataModel.setProperty("/ViewActionsEnabled", true);
            }
            this.getView().getModel("iprData").setData(mIprData);
        },

        onToggleComp: function (oEvt) {
            var oCompCont = oEvt.getSource().getParent().getParent().getBindingContext("iprData"),
                oComp = oCompCont.getModel().getProperty(oCompCont.getPath());
            oCompCont.getModel()
                     .setProperty(oCompCont.getPath() + "/_contentVisible", !oComp._contentVisible);
        },

        _getAppData: function (sProperty) {
            return this.getOwnerComponent().getModel("AppData").getProperty(sProperty);
        },
        _setAppData: function (sPropName, Value) {
            this.getOwnerComponent().getModel("AppData").setProperty(sPropName, Value);
        },

        showUserGuide: function (oEvt) {
            if (!this._oPopover) {
                this._createInstructionPopover()
                    .openBy(oEvt.getSource());
            } else {
                this._oPopover.close();
            }
        },

        _createInstructionPopover: function () {
            this._oPopover = sap.ui.xmlfragment("zhcm_IPR.fragments.UserGuidePopover", this);
            this.getView().addDependent(this._oPopover);
            this._oPopover.attachAfterClose(function () {
                this._oPopover.destroy();
                this._oPopover = null;
            }, this);

            return this._oPopover;
        },

        onBtnBackPress: function (oEvt) {
            var oRouteParams = {};
            if (this._getAppData("/activeTab")) {
                oRouteParams.tabId = this._getAppData("/activeTab");
            } 
            this.getOwnerComponent().getRouter()
                                    .navTo(this._getMobilePrefix() + "Overview", oRouteParams);
        },

        _getPrintform360 : function(sAppraisal, sPernr, sIdEvalProc) {
            if (sAppraisal === null || sAppraisal === undefined || sAppraisal === "") {
                DataAccess.getPrintform360(sPernr, sIdEvalProc);
            } else {
                DataAccess.getReportAnonymityCheck(sPernr, sIdEvalProc, sAppraisal).then(
                    function(oObj) {
                        if (oObj) {
                            if (oObj.FLAG === true) {
                                DataAccess.getPrintform360(oObj.PERNR, oObj.ID_EVAL_PROC);
                            } else {
                                MessageBox.show(
                                    "Недостаточно данных для формирования отчёта. По данной оценке менее 3-х оцененных анкет.", 
                                    {
                                        icon: MessageBox.Icon.INFORMATION,
                                        title: "Информация",
                                        actions: [MessageBox.Action.OK]
                                    });
                            }
                        }
                    },
                    function(oError) {
                        console.log("iprBase._getPrintform360('getReportAnonymityCheck') > Error: ", oError);
                    }
                );
            }
        },

        onReport360: function () {
            var oIprData = this.getView().getModel("iprData").getData();
            DataAccess.getPrintform360(oIprData.PERNR, oIprData.ID_EVAL_PROC);
         },

        _refreshFeedData: function(that, oFeedData) {
            if (oFeedData.length > 0) {
                var aFeeds = [];
                var aFeedAPStatuses = [];

                // смотрим какие AP_STATUS есть в коллекции комментариев и формируем из них массив
                oFeedData.forEach(function(feed) {
                    if (aFeedAPStatuses.indexOf(feed.AP_STATUS) === -1) {
                        aFeedAPStatuses.push(feed.AP_STATUS);
                    }
                });

                // берем массив из AP_STATUS, смотрим какие и собираем на его основе массив комментариев сгруппированный по нему
                aFeedAPStatuses.forEach(function(status) {
                    var oFeed = {
                        STATUS_NAME: '',
                        STATUS: status,
                        FEEDS: []
                    };
                    oFeedData.forEach(function(feed) {
                        if (feed.AP_STATUS == status) {
                            oFeed.STATUS_NAME = feed.AP_STATUS_NAME;
                            oFeed.FEEDS.push(feed);
                        }
                    });
                    aFeeds.push(oFeed);
                });

                // сортируем массив комментариев по AP_STATUS от большего к меньшему (5 -> 4 -> 3-> 2-> 1)
                aFeeds.sort(function(a, b) {
                    if (Number(a.STATUS) < Number(b.STATUS)) {
                        return 1;
                    } else {
                        return -1;
                    }
                });

                // заполняем модель
                that.getView().getModel("feedItems").setData(aFeeds);
            }
        },

        onFeedPost: function (oEvt) {
            var sValue = oEvt.getParameter("value"),
                that = this,
                oFeedList = this.getView().byId("idFeedList"),
                mComment = {
                    PERNR_MEMID: this._getAppData("/CurrentPernrMemId"),
                    APPRAISAL_ID: this.getView().getModel("iprData").getProperty("/APPRAISAL_ID"),
                    TEXT: sValue
                };

            oFeedList.setBusy(true);

            DataAccess.sendFeedComment(mComment)
                .then(
                    function (oSuccess) {
                        DataAccess.getFeed(oSuccess.APPRAISAL_ID)
                            .then(function (oFeedData) {
                                    that._refreshFeedData(that, oFeedData);
                                    oFeedList.setBusy(false);
                                },
                                function (oError) {
                                    oFeedList.setBusy(false);
                                });
                    },
                    function (oError) {
                        MessageBox.error(oError, {title: "Ошибка"});
                        oFeedList.setBusy(false);
                    });
        },

        handleMessagePopoverPress: function (oEvent) {
			oMessagePopover.toggle(oEvent.getSource());
		},

		_clearDataMessagePopover: function() {
            var oData = [],
                viewModel = new JSONModel(),
                mErrorBorder = this.getView().getModel("mErrorBorder"),
                oErrorBorderUICtrls = $(".zErrorBorder"),
                oErrorBorderTileUICtrls = $(".zDevMethItemError"),
                oErrorBorderInput = $(".zDevMethInputError");

            if (mErrorBorder) {
                mErrorBorder.setProperty("/hasErrorBorder", false);
                mErrorBorder.setProperty("/Items", []);  
                this.getView().setModel(mErrorBorder, "mErrorBorder");               
            }             

            if (oErrorBorderUICtrls.length > 0) {
                oErrorBorderUICtrls.removeClass("zErrorBorder");
            }
            if (oErrorBorderTileUICtrls.length > 0) {
                oErrorBorderTileUICtrls.removeClass("zDevMethItemError");
            }
            if (oErrorBorderInput) {
            	oErrorBorderInput.removeClass("zDevMethInputError");
            }

            viewModel.setData({
                messagesLength: oData.length + ""
            });
            this.getView().setModel(viewModel);

            var oModel = oMessagePopover.getModel();
			if (oModel) {
				oMessagePopover.getModel().setData(oData);
			}
        },

        _getCompetencyUICtrl: function(sCompId) {
            var selector = '[data-compid="' + sCompId + '"]';
            return $(selector);
        },

        _getCompetencyLevelUICtrl: function(sCompId) {
            var selector = '[data-CbLevelCompId="' + sCompId + '"]';
            return $(selector);
        },
        
        _getCompetencyTypeUICtrl: function(sButton, oCompUICtrl, sTypeId) {
            var selector = "";
            switch (sButton) {
                case "ZENDIPR":
                    return $(oCompUICtrl).find($(".zReportTile"));
                default:
                    if (sTypeId && sTypeId !== "00000000") {
                        selector = '[data-devTypeId="' + sTypeId + '"]';
                        return $(selector, oCompUICtrl);
                    }
                    break;
            }
            return null;
        },

        _getDevMethod: function(aCompetencies, sCompId, sTypeId, sRowIID) {
            var oObj = {
                    oDevType: null,
                    oDevMethod: null
                },
                oCompetency = aCompetencies.filter(function(x) {
                        return x.COMP_ID === sCompId;
                    })[0];

            if (oCompetency && oCompetency.DevTypes) {
                oObj.oDevType = oCompetency.DevTypes.filter(function(y) {
                        return y.TYPE_ID === sTypeId;
                    })[0];
                if (oObj.oDevType && oObj.oDevType.DevMethods) {
                    var nRowIID = parseInt(sRowIID, 10);
                    oObj.oDevMethod = oObj.oDevType.DevMethods.filter(function(z) {
                            return parseInt(z.ROW_IID, 10) === nRowIID;
                        })[0];
                }
            }
            return oObj;
        },

        _addItemErrorBorder: function(mErrorBorder, oItem) {
            var arrItems = mErrorBorder.getProperty("/Items") || [];
            if (arrItems.filter(function(el){
                if (el === oItem) {
                    return true;
                }
                return false;
            }).length <= 0) {
                arrItems.push(oItem);

                mErrorBorder.setProperty("/hasErrorBorder", true);
                mErrorBorder.setProperty("/Items", arrItems);  
                this.getView().setModel(mErrorBorder, "mErrorBorder");                
            }
        },

        _setErrorBorderToTextArea: function(oTextArea) {
            if (oTextArea) {
                oTextArea.addClass("zErrorBorder");
            }
        },

        _setErrorBorderToUICtrl: function(sREP, sRowIId, oDevMethod, oCompTypeUICtrl, mErrorBorder) {
            if (oDevMethod) {
                var selector = '[data-rowIId="' + parseInt(sRowIId, 10) + '"]';
                if (parseInt(oDevMethod.SHORT_RES, 10) === 0 && sREP === "S") {
                    var oSeg = $(oCompTypeUICtrl).find(selector);
                    if (oSeg) {
                        var oSegBtn = $(oSeg).find(".zResSegmBtn");
                        if (oSegBtn) {
                            oSegBtn.addClass("zErrorBorder");  
                            this._addItemErrorBorder(mErrorBorder, $(oSegBtn).prop("id"));
                        }
                    }
                }
                if (!oDevMethod.DETAILED_RES && sREP === "D") {
                    var oTextArea = $(selector, oCompTypeUICtrl).find(".zResultText");
                    this._setErrorBorderToTextArea(oTextArea);
                    this._addItemErrorBorder(mErrorBorder, $(oTextArea).prop("id"));
                }
            }
         },

        _messagePopoverSetData: function(aErrorMessages, that) {
            var oModel = oMessagePopover.getModel();
            if ( ! oModel) {
                oModel = new JSONModel();
                oMessagePopover.setModel(oModel);
            }

            var oData = aErrorMessages.map(function (data) {
                return {
                    type: "Error",
                    title: data.TEXT,
                    activeTitle: true,
                    enabled: true,
                    subtitle: data.subtitle,
                    description: data.description,
                    active: true,
                    group: "General"
                };
            });

            var viewModel = new JSONModel();
            viewModel.setData({
                messagesLength: oData.length + ""
            });

            that.getView().setModel(viewModel);
            oMessagePopover.getModel().setData(oData);
            that.getView().byId("messagePopoverBtn").firePress();
        },
        
        _setErrorBorderInputWeight: function(that, sButton, mErrorBorder, aCompetencies, sRowIID, oErrorMessage) {
            var nRowIID = parseInt(sRowIID, 10) || 0;
            for (var k = 0; k < aCompetencies.length; k++) {
                var oCompetency = aCompetencies[k],
                	sCompId = oCompetency.COMP_ID,
                    oCompUICtrl = that._getCompetencyUICtrl(sCompId);
                if (oCompUICtrl) {
                    for(var g = 0; g < oCompetency.DevTypes.length; g++) {
                        var oDevType = oCompetency.DevTypes[g];
                        for (var f = 0; f < oDevType.DevMethods.length; f++) {
                            var oDevMethod = oDevType.DevMethods[f],
                                selector = '[data-devTypeId="' + oDevType.TYPE_ID + '"]',
                                oDevTypeUICtrl = $(selector, oCompUICtrl);
                            if (parseInt(oDevMethod.ROW_IID, 10) === nRowIID) {
                                if (oDevTypeUICtrl) {
                                    selector = '[data-devMethRowIID="' + sRowIID.substr(4) + '"] input';
                                    var oDevMethUICtrl = $(selector, oDevTypeUICtrl);
                                    if (oDevMethUICtrl) {
                                        if (oDevMethUICtrl.addClass) {
                                            oDevMethUICtrl.addClass("zDevMethInputError");
                                        } else {
                                            oDevMethUICtrl.className += " zDevMethInputError";
                                        }
                                        that._addItemErrorBorder(mErrorBorder, $(oDevMethUICtrl).prop("id"));
                                        return;
                                    } 
                                }                            
                            }
                        }
                    }
                }
            }     
            that._setErrorBorderAnonymousInputWeight(that, 
                sButton, mErrorBorder, aCompetencies, oErrorMessage);
        },
        
        _setErrorBorderAnonymousInputWeight: function(that, sButton, mErrorBorder, aCompetencies, oErrorMessage) {
        	var str = oErrorMessage.TEXT;
        	str = str.replace("Укажите вес для способа развития  ", "").trim();
			for (var k = 0; k < aCompetencies.length; k++) {
				var oCompetency = aCompetencies[k],
					sCompId = oCompetency.COMP_ID,
					oDevTypes = oCompetency.DevTypes;
				for (var i = 0; i < oDevTypes.length; i++) {
					var sTypeId = oDevTypes[i].TYPE_ID,
						oDevMethods = oDevTypes[i].DevMethods;
                    for (var j = 0; j < oDevMethods.length; j++) {
                    	if (oDevMethods[j].METH_NAME.indexOf(str) > -1) {
                    		var oCompUICtrl = that._getCompetencyUICtrl(sCompId);
                    		for (var g = 0; g < oCompUICtrl.length; g++) {
                            	var oCompUICtrlItem = oCompUICtrl[g],
                            		oCompTypeUICtrl = that._getCompetencyTypeUICtrl(sButton, oCompUICtrlItem, sTypeId);
                    			if (oCompTypeUICtrl) {
                    				var	divsInput =	$(".zMethodWeight", oCompTypeUICtrl);
                    				if (divsInput) {
                    					for (var f = 0; f < divsInput.length; f++) {
                    						if (!divsInput[f].dataset.devmethrowiid) {
                    							var oDivUICtrl = divsInput[f];
                    							var	oInputUICtrl =	$("input", oDivUICtrl);
                    							if (oInputUICtrl.addClass) {
			                                        oInputUICtrl.addClass("zDevMethInputError");
			                                    } else {
			                                        oInputUICtrl.className += " zDevMethInputError";
			                                    }
			                                    that._addItemErrorBorder(mErrorBorder, $(oInputUICtrl).prop("id"));
			                                    return;
                    						}
                    					}
                    				}
                    			} 
                    		}
                    	}
                    } 
				}
			}
        },

        // Выделить все поля ввода "Веса" 
        _setErrorBorderAllInputWeight: function(that, mErrorBorder) {
            var selector = '[data-devMethRowIID] input',
            	oInputUICtrls = $(selector, $("#" + that.oView.sId));
        	if (oInputUICtrls) {
        		for (var i = 0; i < oInputUICtrls.length; i++) {
        			var oUICtrl = oInputUICtrls[i];
        			if (oUICtrl.addClass) {
                        oUICtrl.addClass("zDevMethInputError");
                    } else {
                        oUICtrl.className += " zDevMethInputError";
                    }
                    that._addItemErrorBorder(mErrorBorder, $(oUICtrl).prop("id"));
        		}
        	}
        },  

        _setErrorBorderCompLevel: function(that, sCompId, mErrorBorder) {
            var oCompLevelUICtrl = that._getCompetencyLevelUICtrl(sCompId);
            if (oCompLevelUICtrl) {
                if (oCompLevelUICtrl.addClass) {
                    oCompLevelUICtrl.addClass("zDevMethInputError");
                } else {
                    oCompLevelUICtrl.className += " zDevMethInputError";
                }
                that._addItemErrorBorder(mErrorBorder, $(oCompLevelUICtrl).prop("id"));                                      
            }
        },

        _setErrorBorderMethodTile: function(that, sCompId, sRowIID, mErrorBorder, aCompetencies) {
            for (var i = 0; i < aCompetencies.length; i++) {
                if (aCompetencies[i].COMP_ID === sCompId) {
                    var oCompUICtrl = that._getCompetencyUICtrl(sCompId),
                    	oDevTypes = aCompetencies[i].DevTypes;
                    for (var j = 0; j < oDevTypes.length; j ++) {
                        var oDevType = oDevTypes[j],
                            oDevMethods = oDevType.DevMethods;
                        for (var k = 0; k < oDevMethods.length; k ++) {
                            var oDevMethod = oDevMethods[k];
                            if (parseInt(oDevMethod.ROW_IID, 10) === parseInt(sRowIID, 10)) {
                                var selector = '[data-devTypeId="' + oDevType.TYPE_ID + '"]',
                                    oDevTypeUICtrl = $(selector, oCompUICtrl);
                                if (oDevTypeUICtrl) {
                                    selector = '[data-devTileMethRowIID="' + sRowIID.substr(4) + '"]';
                                    var oDevMethUICtrl = $(selector, oDevTypeUICtrl);
                                    if (oDevMethUICtrl) {
                                        if (oDevMethUICtrl.addClass) {
                                            oDevMethUICtrl.addClass("zDevMethItemError");
                                        } else {
                                            oDevMethUICtrl.className += " zDevMethItemError";
                                        }
                                        that._addItemErrorBorder(mErrorBorder, $(oDevMethUICtrl).prop("id"));
                                        return;
                                    } 
                                }
                            }
                        }
                    }
                }
            }
        },

        _setErrorBorderInputPercent: function(that, mErrorBorder, aCompetencies, sRowIID) {
            for (var i = 0; i < aCompetencies.length; i++) {
                var oCompUICtrl = that._getCompetencyUICtrl(aCompetencies[i].COMP_ID),
                    oDevTypes = aCompetencies[i].DevTypes;
                for (var j = 0; j < oDevTypes.length; j ++) {
                    var oDevType = oDevTypes[j],
                    oDevMethods = oDevType.DevMethods;
                    for (var k = 0; k < oDevMethods.length; k ++) {
                        var oDevMethod = oDevMethods[k];
                        if (parseInt(oDevMethod.ROW_IID, 10) === parseInt(sRowIID, 10)) {
                            var selector = '[data-devTypeId="' + oDevType.TYPE_ID + '"]',
                                oDevTypeUICtrl = $(selector, oCompUICtrl);
                            if (oDevTypeUICtrl) {
                                selector = '[data-devTileMethRowIID="' + sRowIID.substr(4) + '"] input';
                                var oDevMethUICtrl = $(selector, oDevTypeUICtrl);
                                if (oDevMethUICtrl) {
                                    if (oDevMethUICtrl.addClass) {
                                        oDevMethUICtrl.addClass("zDevMethInputError");
                                    } else {
                                        oDevMethUICtrl.className += " zDevMethInputError";
                                    }
                                    that._addItemErrorBorder(mErrorBorder, $(oDevMethUICtrl).prop("id"));
                                    return;
                                } 
                            }
                        }
                    } 
                }               
            }
        },

        _setErrorBorderInputDate: function(that, oCompUICtrl, mErrorBorder, aCompetencies, sCompId, sTypeId, sRowIID) {
            for (var i = 0; i < aCompetencies.length; i++) {
                if (aCompetencies[i].COMP_ID === sCompId) {
                    var aDevTypes = aCompetencies[i].DevTypes;
                    for (var j = 0; j < aDevTypes.length; j ++) {
                        if (aDevTypes[j].TYPE_ID === sTypeId) {
                            var aDevMethods = aDevTypes[j].DevMethods;
                            for (var k = 0; k < aDevMethods.length; k ++) {
                                if (parseInt(aDevMethods[k].ROW_IID, 10) === parseInt(sRowIID, 10)) {
                                    var selector = '[data-devTypeId="' + sTypeId + '"]',
                                        oDevTypeUICtrl = $(selector, oCompUICtrl);
                                    if (oDevTypeUICtrl) {
                                        selector = '[data-devTileMethRowIID="' + sRowIID.substr(4) + '"] input[role="combobox"]';
                                        var oDevMethUICtrl = $(selector, oDevTypeUICtrl);
                                        if (oDevMethUICtrl) {
                                            if (oDevMethUICtrl.addClass) {
                                                oDevMethUICtrl.addClass("zDevMethInputError");
                                            } else {
                                                oDevMethUICtrl.className += " zDevMethInputError";
                                            }
                                            that._addItemErrorBorder(mErrorBorder, $(oDevMethUICtrl).prop("id"));
                                            return;
                                        } 
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        

        /* 
        oErrorMessage.NUM:
        - 021 - Выделить 'Уровень развития' у компетенции
        - 022 - Выделить всю плитку
        - 023 - Выделить 'Вес' у метода по ROW_IID
        - 024 - Выделить все веса
        */
        _highlightUICtrlWithInputError: function(that, aErrorMessages, sButton) {
            var aCompetencies = that.getView().getModel("iprData").getData().Competencies;
            if (aCompetencies) {
                var mErrorBorder = this.getView().getModel("mErrorBorder");

                for (var i = 0; i < aErrorMessages.length; i++) {
                    var oErrorMessage = aErrorMessages[i],
                        sCompId = oErrorMessage.COMP_ID,
                        sTypeId = oErrorMessage.TYPE_ID,
                        sRowIID = oErrorMessage.SP_ID,
                        sREP = oErrorMessage.REP,
                        num = parseInt(oErrorMessage.NUM, 10) || 0;

                    if (sCompId && sCompId !== "00000000") {
                        var oCompUICtrl = that._getCompetencyUICtrl(sCompId);
                        if (oCompUICtrl) {
                            if (num === 19) {
                                that._setErrorBorderInputDate(that, 
                                    oCompUICtrl, mErrorBorder, aCompetencies, sCompId, sTypeId, sRowIID);
                            } else if (num === 21) {
                                that._setErrorBorderCompLevel(that, sCompId, mErrorBorder);
                            } else if (num === 22) {
                                that._setErrorBorderMethodTile(that, sCompId, sRowIID, mErrorBorder, aCompetencies);
                            } else if (num === 23 && sTypeId === "00000000") {
                                that._setErrorBorderAnonymousInputWeight(that, 
                                    sButton, mErrorBorder, aCompetencies, oErrorMessage);
                            } else {
                                for (var j = 0; j < oCompUICtrl.length; j ++) {
                                    var oCompUICtrlItem = oCompUICtrl[j],
                                    	oCompTypeUICtrl = that._getCompetencyTypeUICtrl(sButton, oCompUICtrlItem, sTypeId);
                                    if (oCompTypeUICtrl) {
                                        var oObj = that._getDevMethod(aCompetencies, sCompId, sTypeId, sRowIID);
                                        if (oObj && oObj.oDevMethod) {
                                            var oDevMethod = oObj.oDevMethod;
                                            oErrorMessage.subtitle = "";
                                            if (oObj.oDevType) {
                                                oErrorMessage.subtitle = oObj.oDevType.TYPE_NAME;
                                            }
                                            oErrorMessage.description = "";
                                            if (oDevMethod) {
                                                oErrorMessage.description = oDevMethod.METH_NAME
                                                    .replace(/[\r\n]/g, " ")
                                                    .replace(/[\n\r]/g, " ")
                                                    .replace(/[\r]/g, " ")
                                                    .replace(/[\n]/g, " ");
                                            }
                                             that._setErrorBorderToUICtrl(sREP, sRowIID, oDevMethod, oCompTypeUICtrl, mErrorBorder);
                                        } else {
                                            if (oCompTypeUICtrl) {
                                                if (oCompTypeUICtrl.addClass) {
                                                    oCompTypeUICtrl.addClass("zErrorBorder");
                                                } else {
                                                    oCompTypeUICtrl.className += " zErrorBorder";
                                                }
                                                that._addItemErrorBorder(mErrorBorder, $(oCompTypeUICtrl).prop("id"));    
                                            }
                                        }
                                    }
                                    else {
                                        if (oCompUICtrlItem.addClass) {
                                            oCompUICtrlItem.addClass("zErrorBorder");
                                        } else {
                                            oCompUICtrlItem.className += " zErrorBorder";
                                        }
                                        that._addItemErrorBorder(mErrorBorder, $(oCompUICtrlItem).prop("id"));
                                    }
                                }
                            }
                        }
                    } else  if (num === 26 && sTypeId === "00000000") {
                        that._setErrorBorderInputPercent(that, mErrorBorder, aCompetencies, sRowIID);
                    } else {
                        if (oErrorMessage.REP === "O") {
                            var oTextArea = $(".zOverallTextArea");
                            that._setErrorBorderToTextArea(oTextArea);
                            that._addItemErrorBorder(mErrorBorder, $(oTextArea).prop("id"));
                        } else {
                            // бизнес-чек и рамки
                            var nRowIID = parseInt(sRowIID, 10) || 0;
                            if (nRowIID === 0) {
                                // oErrorMessage.NUM = 24, Выделить все поля ввода веса
                                that._setErrorBorderAllInputWeight(that, mErrorBorder);
                            } else {
                                // oErrorMessage.NUM = Выделить 'Вес' у метода по ROW_IID
                                that._setErrorBorderInputWeight(that, sButton, mErrorBorder, aCompetencies, sRowIID, oErrorMessage);
                            }
                        }
                    }
                }
            }

            that._messagePopoverSetData(aErrorMessages, that);
        },

        _processSend: function (sButton) {
            this.getView().setBusy(true);
            var that = this;

            this._clearDataMessagePopover();

            var oSendData = this._parseBeforeSend();
            oSendData.BUTTON = sButton;
            DataAccess.sendIprData(oSendData)
                .then(
                    function (oSuccess) {
                        that.getView().setBusy(false);
                        if (sButton != "") {
                            that.onBtnBackPress();
                        } else {
                            sap.m.MessageToast.show("Сохранено");
                        }
                    },
                    function (aErrorMessages) {
                        that.getView().setBusy(false);
                        if (aErrorMessages.length > 0) {
                            that._highlightUICtrlWithInputError(that, aErrorMessages, sButton);
                        } else {
                            MessageBox.error(aErrorMessages.message, {title: "Ошибка"});
                        }
                    }
                );
        },

        _parseBeforeSend: function () {
        	var oIprData = this.oView.getModel("iprData").getData();
            var oParsed = $.extend(true, {}, oIprData);
            $.each(oParsed.Competencies, function (sInd, oComp) {
                delete oComp._contentVisible;
                delete oComp._compVisible;
                $.each(oComp.DevTypes, function(sInd, oDevType) {
                    delete oDevType._expanded;
                    $.each(oDevType.DevMethods, function(nIndex, oDevMethod) {
                        if (!oDevMethod.PRC || isNaN(parseFloat(oDevMethod.PRC, 10))) {
                            delete oDevMethod.PRC;
                        }
                        if (!oDevMethod.UNTIL) {
                            delete oDevMethod.UNTIL;
                        }
                    });
                });
            });
            delete oParsed.APPROVE_INVERT;
            return oParsed;
        },

        onToggleHeader: function () {
            var oViewData = this.getView().getModel("viewData");
            oViewData.setProperty("/HeaderExpanded", !oViewData.getProperty("/HeaderExpanded"));
        },
        
        _getMobilePrefix: function () {
            if (this.getOwnerComponent().getModel("device").getProperty("/isPhone")) {
                return "m";                
            } else {
                return "";
            }
        },

        _bindJSONModel: function (sModelName, oData) {
            var oModel = this.getView().getModel(sModelName);
            if (!oModel) {
                oModel = new JSONModel(oData);
                this.getView().setModel(oModel, sModelName);
            } else {
                oModel.setData(oData);
            }
        },

        _getMyIPR: function () {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getMyIpr(this._getAppData("/CurrentPernrMemId"));
            oDef.done(function (oMyIprData) {
                var bMyIprVisible = oMyIprData.length > 0;
                that._bindJSONModel("MyIPR", oMyIprData);
                oViewModel.setProperty("/MyIprTabVisible", bMyIprVisible);
                oViewModel.setProperty("/MyIprSTabsVisible", bMyIprVisible);
                oViewModel.setProperty("/TxtVisible", !bMyIprVisible);
                if (!bMyIprVisible) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getReportProcSet: function () {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getReportProcSet();
            oDef.done(function (oReportProcSet) {
                var bReportProcSet = oReportProcSet.length > 0;
                that._bindJSONModel("ReportProcSet", oReportProcSet);
                oViewModel.setProperty("/ReportProcSet", oReportProcSet);
                if (!bReportProcSet) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getReportOrgehSet: function (sIdDevProc) {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getReportOrgehSet(sIdDevProc);
            oDef.done(function (oReportOrgehSet) {
                var bReportOrgehSet = oReportOrgehSet.length > 0;
                that._bindJSONModel("ReportOrgehSet", oReportOrgehSet);
                oViewModel.setProperty("/ReportOrgehSet", oReportOrgehSet);
                if (!bReportOrgehSet) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getReportFioSet: function (sIdDevProc) {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getReportFioSet(sIdDevProc);
            oDef.done(function (oReportFioSet) {
                var bReportFioSet = oReportFioSet.length > 0;
                that._bindJSONModel("ReportFioSet", oReportFioSet);
                oViewModel.setProperty("/ReportFioSet", oReportFioSet);
                if (!bReportFioSet) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getReportDevTypeSet: function (sIdDevProc) {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getReportDevTypeSet(sIdDevProc);
            oDef.done(function (oReportDevTypeSet) {
                var bReportDevTypeSet = oReportDevTypeSet.length > 0;
                that._bindJSONModel("ReportDevTypeSet", oReportDevTypeSet);
                oViewModel.setProperty("/ReportDevTypeSet", oReportDevTypeSet);
                if (!bReportDevTypeSet) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },
        
        _getTutorShips: function () {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getTutorShips(this._getAppData("/CurrentPernrMemId"));
            oDef.done(function (oMyIprData) {
                var bMyTutorShips = oMyIprData.length > 0;
                that._bindJSONModel("MyTutorShips", oMyIprData);
                oViewModel.setProperty("/MyTutorShips", bMyTutorShips);
                if (!bMyTutorShips) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getSubDivs: function () {
            var that = this;
            var oViewModel = this.getView().getModel("viewData");
            var oDef = DataAccess.getSubDivs(this._getAppData("/CurrentPernrMemId"));
            oDef.done(function (oMyIprData) {
                var bMyTutorShips = oMyIprData.length > 0;
                that._bindJSONModel("MySubDivs", oMyIprData);
                oViewModel.setProperty("/MySubDivs", bMyTutorShips);
                if (!bMyTutorShips) {
                    oViewModel.setProperty("/NoData", "Нет данных для отображения");
                }
            });
            return oDef;
        },

        _getMySubords: function (bChangeTabVisibility, bUpdateSubords, bUpdateSubordsFilter) {
            var that = this;
            if (bUpdateSubordsFilter) {
                this._bindJSONModel("MySubordsUnique", []);
                this._setAppData("/overviewFilters/Subord", "");
                this._setAppData("/overviewFilters/MySubordsFilterLoading", true);
            }

            var oDef = DataAccess.getMySubords(this._getAppData("/overviewFilters/PernrMemId"));
            oDef.done(function (aSubords) {
                var aUnqiquePernrs = [];
                var aUniqueSubords = [];

                $.each(aSubords, function (iInd, oSubord) {
                    if (aUnqiquePernrs.indexOf(oSubord.PERNR) == -1) {
                        aUniqueSubords.push(oSubord);
                        aUnqiquePernrs.push(oSubord.PERNR);
                    }
                });
                aUniqueSubords.unshift({});
                if (bUpdateSubords) {
                    that._bindJSONModel("MySubords", aSubords);
                }
                if (bUpdateSubordsFilter) {
                    that._bindJSONModel("MySubordsUnique", aUniqueSubords);
                }
                if (bChangeTabVisibility) {
                    that.getView().getModel("viewData").setProperty("/SubordsTabVisible", aSubords.length > 0);
                }
                that._setAppData("/overviewFilters/MySubordsFilterLoading", false);
            });
            return oDef;
        },

        onAfterRendering: function() {
            var mErrorBorder = this.getView().getModel("mErrorBorder");
            if (mErrorBorder) {
                var bFlag = mErrorBorder.getProperty("/hasErrorBorder") || false,
                    arrItems = mErrorBorder.getProperty("/Items") || [];
                if (bFlag) {
                    $.each(arrItems, function(sInd, sItemId) {
                        var selector = "#" + sItemId,
                            oUICtrl = $(selector);
                        if (oUICtrl) {
                            var ctrl;
                            if (oUICtrl.prop("nodeName") === "UL") {
                                ctrl = $(oUICtrl).find(".sapMSegBBtnSel");
                                if (ctrl.length > 0) {
                                    var nIndex = ctrl[0].getAttribute("aria-posinset") || -1,
                                        n = parseInt(nIndex, 10);
                                    if (n === 2) {
                                        oUICtrl.addClass("zErrorBorder");
                                    }
                                }
                            } else if (oUICtrl.prop("nodeName") === "DIV") {
                                ctrl = $(oUICtrl).find("textarea");
                                if (ctrl.length > 0) {
                                    if ($(ctrl[0]).val() === "") {
                                        oUICtrl.addClass("zErrorBorder");
                                    }
                                }
                            }
                        }
                    });
                }
            }
        },

        _inquiryEnterCommentForEmployee: function(sButton) {
            var that = this;
            var dialog = new sap.m.Dialog({
                title: "Вернуть на доработку?",
                type: "Message",
                content: [
                    new sap.m.Text({text: "Укажите, какие корректировки следует внести в план развития сотруднику:"}),
                    new sap.m.TextArea("returnTextArea", {
                        liveChange: function (oEvent) {
                            var sText = oEvent.getParameter("value");
                            var parent = oEvent.getSource().getParent();
                            parent.getBeginButton().setEnabled(sText.length > 0);
                        },
                        width: "100%",
                        height: "5.6rem",
                        placeholder: "Введите комментарий для сотрудника"
                    }).addStyleClass("ZHCM_PM_0681").addStyleClass("zReturnTextArea")
                ],
                beginButton: new sap.m.Button({
                    text: "Отправить",
                    type: "Emphasized",
                    enabled: false,
                    press: function () {
                        var sText = sap.ui.getCore().byId("returnTextArea").getValue();
                        dialog.close();
                        that.getView().getModel("iprData").setProperty("/RETURN_COMMENT", sText);
                        that._processSend(sButton);
                    }
                }),
                endButton: new sap.m.Button({
                    text: "Отменить",
                    type: "Reject",
                    press: function () {
                        dialog.close();
                    }
                }),
                afterClose: function () {
                    dialog.destroy();
                }
            });

            dialog.open();
        },
        onTutorItemPress: function(oEvent) {
            var oContext = oEvent.getSource().getBindingContext("viewData"),
                oSelectedTutor = oContext.getObject(),
                oModel = this.getView().getModel("iprData"),
            	oViewData = oModel.getData();

            if (oViewData && oSelectedTutor) {
            	oViewData.TUTOR_FIO = oSelectedTutor.TUTOR_FIO;
                oViewData.TUTOR_PERNR = oSelectedTutor.TUTOR_PERNR;
                oViewData.TUTOR_ORGEH = oSelectedTutor.Orgeh;
                oViewData.TUTOR_ORGEH_TXT = oSelectedTutor.OrgehTxt;
                oViewData.TUTOR_PLANS_TXT = oSelectedTutor.PlansTxt;
                oModel.setData(oViewData);
                oModel.refresh(true);
            }
            
            this._oTutorDialog.close();             
        },

        onValueFIOHelpRequested: function(oEvent) {
            if (!this._oTutorDialog) {
                if (this._getMobilePrefix() === "m") {
                    this._oTutorDialog = sap.ui.xmlfragment("tutorDialog", "zhcm_IPR.fragments.mobile.TutorDialog", this);
                } else {
                    this._oTutorDialog = sap.ui.xmlfragment("tutorDialog", "zhcm_IPR.fragments.TutorDialog", this);
                }                
                this.getView().addDependent(this._oLibraryDialog);
            }    

            var oViewData = this.getView().getModel("iprData");  
            this._oTutorDialog.setModel(oViewData, "viewData");

            this._oTutorDialog.open();
        },

        handleTutorClose: function(oEvent) {
            this._oTutorDialog.close();
        },
        

    });
});